-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-01-2015 a las 19:50:41
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `kinja`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
`id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `urlPic` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `blog_padre` int(11) DEFAULT NULL,
  `main` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`id`, `nombre`, `urlPic`, `descripcion`, `blog_padre`, `main`) VALUES
(1, 'Manolito', 'assets/images/blogs/gawker-logo.png', 'Super Blog de superman', NULL, 1),
(2, 'Jalopnik', 'assets/images/blogs/jalopnik-logo.png', 'Blog de coches', NULL, 1),
(3, 'Secundario1', NULL, 'Un blog de mala muerte donde no entra nadie', 1, 0),
(5, 'Super', NULL, 'Blog personal del usuario Super', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs_usuario`
--

CREATE TABLE IF NOT EXISTS `blogs_usuario` (
`id` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `autor` int(11) NOT NULL,
  `personal` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `blogs_usuario`
--

INSERT INTO `blogs_usuario` (`id`, `blog`, `autor`, `personal`) VALUES
(1, 5, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE IF NOT EXISTS `comentario` (
`id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `urlContenido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `autor` int(11) NOT NULL,
  `noticia` int(11) NOT NULL,
  `replyTo` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `fecha`, `urlContenido`, `autor`, `noticia`, `replyTo`) VALUES
(1, '2015-01-08 18:33:23', 'assets/content/comments/1.txt', 2, 1, NULL),
(2, '2015-01-08 18:33:23', 'assets/content/comments/2.txt', 1, 3, NULL),
(3, '2015-01-08 22:53:25', 'assets/content/comments/3.txt', 3, 1, 1),
(11, '2015-01-10 20:06:38', 'assets/content/comments/5.txt', 4, 1, 1),
(12, '2015-01-10 20:06:53', 'assets/content/comments/5.txt', 4, 1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compartidas`
--

CREATE TABLE IF NOT EXISTS `compartidas` (
`id` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `noticia` int(11) NOT NULL,
  `usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
`id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cabecera` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `urlContenido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `urlImagen` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `cabecera`, `urlContenido`, `fecha`, `autor`, `blog`, `urlImagen`) VALUES
(1, 'Ha muerto Franco', 'Ha muerto el bajito gordo con la voz más aguda de España.', 'assets/content/articles/1.txt', '2015-01-08 14:47:15', 1, 1, 'assets/images/articles/1.png'),
(2, 'Mariano Rajoy confirma que es homosexual', 'El Presidente del Gobierno de todos los españoles acaba de salir del armario', 'assets/content/articles/2.txt', '2015-01-08 14:47:15', 1, 1, 'assets/images/articles/2.png'),
(3, 'Ha salido a la venta un nuevo Citröen 2CV', 'Ahora cuenta con tecnología híbrida y tracción a las cuatro ruedas.', 'assets/content/articles/3.txt', '2015-01-08 17:32:42', 2, 2, 'assets/images/articles/3.png'),
(4, 'Desarticulada una red de tráfico de cepillos de dientes', 'La red robaba cepillos de dientes en comercios chinos y los revendía a la mitad de precio.', 'assets/content/articles/4.txt', '2015-01-08 22:14:43', 2, 1, 'assets/images/articles/4.png'),
(5, 'Noticia aleatoria', 'Nobody expects the Spanish Inquisition!', 'assets/content/articles/5.txt', '2015-01-09 12:49:28', 3, 1, NULL),
(6, 'Encontrado el huevo que perdió Franco en África', 'Una excavación reciente ha encontrado lo que parece ser los restos arqueológicos del huevo que le faltaba a Franco.', 'assets/content/articles/6.txt', '2015-01-09 21:45:45', 3, 3, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
`id` int(11) NOT NULL,
  `alias` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `urlPic` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fechaNac` date DEFAULT NULL,
  `sexo` set('hombre','mujer') COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `alias`, `email`, `password`, `admin`, `urlPic`, `nombre`, `apellidos`, `fechaNac`, `sexo`) VALUES
(1, 'Pepe', 'pepe@gmail.com', '1234', 0, 'assets/images/users/pepe-avatar.png', 'Pepe', 'Palomo', NULL, 'hombre'),
(2, 'Papadopoulos', 'papa_dopoulos@gmail.com', '1234', 0, NULL, 'María', 'Esteban', NULL, 'mujer'),
(3, 'Pollo', 'pollo@gmail.com', '1234', 0, 'assets/images/users/pollo-avatar.png', 'Pollo', 'Pollito', NULL, NULL),
(4, 'cphcarolina', 'cphcarolina@gmail.com', 'qwe', 1, NULL, 'Carolina', 'Prada', '1987-10-06', 'mujer'),
(6, 'superman', 'superman@gmail.com', 'qwe', 0, NULL, 'Super', 'Man', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blogs`
--
ALTER TABLE `blogs`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_blog` (`blog_padre`), ADD KEY `nombre` (`nombre`);

--
-- Indices de la tabla `blogs_usuario`
--
ALTER TABLE `blogs_usuario`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_blog_bu` (`blog`), ADD KEY `fk_usuario_bu` (`autor`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_noticias_comentario` (`noticia`), ADD KEY `fk_comentario_reply` (`replyTo`), ADD KEY `fk_autor` (`autor`);

--
-- Indices de la tabla `compartidas`
--
ALTER TABLE `compartidas`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_noticia_compartidas` (`noticia`), ADD KEY `fk_blog_compartidas` (`blog`), ADD KEY `fk_usuario_compartidas` (`usuario`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_autor_noticias` (`autor`), ADD KEY `fk_blogs_noticias` (`blog`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blogs`
--
ALTER TABLE `blogs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `blogs_usuario`
--
ALTER TABLE `blogs_usuario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `compartidas`
--
ALTER TABLE `compartidas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `blogs`
--
ALTER TABLE `blogs`
ADD CONSTRAINT `fk_blog` FOREIGN KEY (`blog_padre`) REFERENCES `blogs` (`id`);

--
-- Filtros para la tabla `blogs_usuario`
--
ALTER TABLE `blogs_usuario`
ADD CONSTRAINT `fk_blog_bu` FOREIGN KEY (`blog`) REFERENCES `blogs` (`id`),
ADD CONSTRAINT `fk_usuario_bu` FOREIGN KEY (`autor`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
ADD CONSTRAINT `fk_autor_comentario` FOREIGN KEY (`autor`) REFERENCES `usuario` (`id`),
ADD CONSTRAINT `fk_comentario_reply` FOREIGN KEY (`replyTo`) REFERENCES `comentario` (`id`),
ADD CONSTRAINT `fk_noticias_comentario` FOREIGN KEY (`noticia`) REFERENCES `noticias` (`id`);

--
-- Filtros para la tabla `compartidas`
--
ALTER TABLE `compartidas`
ADD CONSTRAINT `fk_blog_compartidas` FOREIGN KEY (`blog`) REFERENCES `blogs` (`id`),
ADD CONSTRAINT `fk_noticia_compartidas` FOREIGN KEY (`noticia`) REFERENCES `noticias` (`id`),
ADD CONSTRAINT `fk_usuario_compartidas` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
ADD CONSTRAINT `fk_autor_noticias` FOREIGN KEY (`autor`) REFERENCES `usuario` (`id`),
ADD CONSTRAINT `fk_blogs_noticias` FOREIGN KEY (`blog`) REFERENCES `blogs` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
