-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-01-2015 a las 15:00:47
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `kinja`
--

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`id`, `nombre`, `urlPic`, `descripcion`, `blog_padre`, `main`) VALUES
(1, 'Gawker', 'assets/images/blogs/gawker-logo.png', 'The definitive news and gossip sheet for followers of entertainment, media, and business.', NULL, 1),
(2, 'Jalopnik', 'assets/images/blogs/jalopnik-logo.png', 'Blog de coches', NULL, 1),
(3, 'Secundario1', NULL, 'Un blog de mala muerte donde no entra nadie', 1, 0),
(4, 'Blog del tito Poulos', NULL, 'Mola mazo', NULL, 0),
(5, 'dodo', NULL, 'Blog personal del usuario dodo', NULL, 0);

--
-- Volcado de datos para la tabla `blogs_usuario`
--

INSERT INTO `blogs_usuario` (`autor`, `blog`, `personal`) VALUES
(1, 3, 0),
(5, 5, 1);

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `fecha`, `urlContenido`, `autor`, `noticia`, `replyTo`) VALUES
(1, '2015-01-08 19:33:23', 'assets/content/comments/1.txt', 2, 1, NULL),
(2, '2015-01-08 19:33:23', 'assets/content/comments/2.txt', 1, 3, NULL),
(4, '2015-01-08 23:53:25', 'assets/content/comments/3.txt', 3, 1, 1),
(5, '2015-01-10 00:40:17', 'assets/content/comments/4.txt', 3, 2, NULL),
(7, '2015-01-10 16:20:08', 'assets/content/comments/5.txt', 2, 2, 5),
(29, '2015-01-10 15:42:47', 'assets/content/comments/5.txt', 3, 3, NULL),
(30, '2015-01-10 15:43:11', 'assets/content/comments/5.txt', 3, 1, NULL),
(31, '2015-01-10 15:44:55', 'assets/content/comments/5.txt', 3, 1, NULL),
(36, '2015-01-10 16:30:20', 'assets/content/comments/5.txt', 2, 1, 30),
(37, '2015-01-10 16:30:55', 'assets/content/comments/5.txt', 2, 1, 4),
(38, '2015-01-10 16:33:00', 'assets/content/comments/5.txt', 2, 1, 31),
(39, '2015-01-10 17:41:40', 'assets/content/comments/5.txt', 1, 1, 38),
(57, '2015-01-10 22:32:22', 'assets/content/comments/863.txt', 2, 2, NULL),
(58, '2015-01-10 22:35:44', 'assets/content/comments/3128.txt', 2, 2, NULL),
(59, '2015-01-10 22:46:10', 'assets/content/comments/7514.txt', 2, 4, NULL),
(60, '2015-01-10 22:49:00', 'assets/content/comments/8498.txt', 2, 2, NULL),
(61, '2015-01-10 22:54:02', 'assets/content/comments/8479.txt', 2, 4, NULL),
(66, '2015-01-10 23:18:11', 'assets/content/comments/7416.txt', 2, 1, NULL),
(67, '2015-01-10 23:18:43', 'assets/content/comments/3287.txt', 2, 5, NULL),
(68, '2015-01-10 23:19:14', 'assets/content/comments/2174.txt', 3, 5, NULL),
(69, '2015-01-10 23:19:40', 'assets/content/comments/4354.txt', 3, 5, NULL),
(70, '2015-01-10 23:20:54', 'assets/content/comments/1981.txt', 3, 6, NULL),
(71, '2015-01-10 23:21:55', 'assets/content/comments/3976.txt', 3, 5, NULL),
(72, '2015-01-10 23:27:10', 'assets/content/comments/7416.txt', 3, 5, 67),
(73, '2015-01-10 23:28:06', 'assets/content/comments/3439.txt', 3, 1, 66),
(74, '2015-01-10 23:54:32', 'assets/content/comments/7274.txt', 3, 4, 59),
(75, '2015-01-11 00:13:32', 'assets/content/comments/7334.txt', 3, 4, 59),
(76, '2015-01-11 00:13:44', 'assets/content/comments/1694.txt', 3, 4, NULL),
(77, '2015-01-11 00:14:00', 'assets/content/comments/6186.txt', 3, 4, 76),
(78, '2015-01-11 00:53:10', 'assets/content/comments/8419.txt', 1, 7, NULL),
(79, '2015-01-11 01:53:31', 'assets/content/comments/2124.txt', 1, 14, NULL),
(80, '2015-01-11 02:00:35', 'assets/content/comments/8148.txt', 1, 14, NULL),
(81, '2015-01-11 02:01:10', 'assets/content/comments/2791.txt', 1, 3, NULL),
(82, '2015-01-11 02:01:23', 'assets/content/comments/9974.txt', 1, 3, 29),
(83, '2015-01-11 04:37:19', 'assets/content/comments/2876.txt', 1, 15, NULL),
(84, '2015-01-11 19:47:25', 'assets/content/comments/5388.txt', 2, 7, 78),
(85, '2015-01-15 00:31:38', 'assets/content/comments/2587.txt', 4, 40, NULL),
(86, '2015-01-15 00:41:41', 'assets/content/comments/4791.txt', 4, 40, 85),
(87, '2015-01-15 04:10:07', 'assets/content/comments/2941.txt', 2, 43, NULL);

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `cabecera`, `urlContenido`, `fecha`, `autor`, `blog`, `urlImagen`, `draft`) VALUES
(1, 'Ha muerto Franco', 'Ha muerto el bajito gordo con la voz más aguda de España.', 'assets/content/articles/1.txt', '2015-01-08 15:47:15', 1, 1, 'assets/images/articles/1.png', 0),
(2, 'Mariano Rajoy confirma que es homosexual', 'El Presidente del Gobierno de todos los españoles acaba de salir del armario', 'assets/content/articles/2.txt', '2015-01-08 15:47:15', 1, 1, 'assets/images/articles/2.png', 0),
(3, 'Ha salido a la venta un nuevo Citröen 2CV', 'Ahora cuenta con tecnología híbrida y tracción a las cuatro ruedas.', 'assets/content/articles/3.txt', '2015-01-08 18:32:42', 2, 2, 'assets/images/articles/3.png', 0),
(4, 'Desarticulada una red de tráfico de cepillos de dientes', 'La red robaba cepillos de dientes en comercios chinos y los revendía a la mitad de precio.', 'assets/content/articles/4.txt', '2015-01-08 23:14:43', 2, 1, 'assets/images/articles/4.png', 0),
(5, 'Noticia aleatoria', 'Nobody expects the Spanish Inquisition!', 'assets/content/articles/5.txt', '2015-01-09 13:49:28', 3, 1, NULL, 0),
(6, 'Encontrado el huevo que perdió Franco en África', 'Una excavación reciente ha encontrado lo que parece ser los restos arqueológicos del huevo que le faltaba a Franco.', 'assets/content/articles/6.txt', '2015-01-09 22:45:45', 3, 3, NULL, 0),
(7, '¿Qué es lorem ipsum?', 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona dedicada a la imprenta) desconocido...', 'assets/content/articles/7.txt', '2015-01-11 00:48:41', 3, 1, 'assets/images/articles/7.png', 0),
(8, 'Otra noticia', 'Latin de la Universidad de Hampden-Sydney en Virginia, encontró una de las palabras más oscuras de la lengua del latín, "consecteur", en un pasaje de Lorem Ipsum, y al seguir leyendo distintos textos del latín, descubrió la fuente indudable.', 'assets/content/articles/8.txt', '2015-01-11 01:27:26', 1, 1, NULL, 0),
(9, 'Porro', 'Porrompompero pero', 'assets/content/articles/9.txt', '2015-01-11 01:29:39', 1, 1, NULL, 0),
(10, 'Una historia sobre el apareamiento del lince austral', 'Interesante de narices. Lorem ipsum... bla bla bla...', 'assets/content/articles/10.txt', '2015-01-11 01:38:38', 3, 1, 'assets/images/articles/10.png', 0),
(11, 'Noticia número 9', 'Pues eso.', 'assets/content/articles/11.txt', '2015-01-11 01:42:49', 2, 1, NULL, 0),
(12, 'Lorem ipsum!!', 'Spam!!!', 'assets/content/articles/12.txt', '2015-01-11 01:44:22', 1, 1, NULL, 0),
(13, 'Pedipalpos puestos a la venta en el mercado de Constantinopla', 'Lorem ipsum... y eso', 'assets/content/articles/13.txt', '2015-01-11 01:46:12', 2, 1, 'assets/images/articles/13.png', 0),
(14, 'Robert Pattinson es declarado el hombre más sexy del planeta', 'Y esta foto claramente lo demuestra.', 'assets/content/articles/14.txt', '2015-01-11 01:52:40', 2, 1, 'assets/images/articles/14.png', 0),
(15, 'Gallo claudio es asesinado', 'Ha sido asesinado al salir de su casa.', 'assets/content/articles/15.txt', '2015-01-11 03:02:18', 3, 1, 'assets/images/articles/15.png', 0),
(16, 'LA-LI-LU-LE-LO', 'Socoorroroorroororororrororo', 'assets/content/articles/9353.txt', '2015-01-11 20:29:44', 2, 1, NULL, 0),
(18, 'Vendo Opel Corsa', 'Déjese usted de mierdas', 'assets/content/articles/8288.txt', '2015-01-11 21:34:01', 2, 1, NULL, 0),
(19, 'Spam fresco', 'Traigo spam señoraaaa!!!', 'assets/content/articles/5347.txt', '2015-01-11 21:37:34', 2, 1, NULL, 0),
(20, 'Más spam', 'Este no tan barato.', 'assets/content/articles/4637.txt', '2015-01-11 21:38:47', 2, 1, NULL, 0),
(21, 'Niños enfurecidos agreden a humanos simiescos inocentes en el Niágara', 'Paren ya el abuso contra los pobres humano-simios!', 'assets/content/articles/9127.txt', '2015-01-11 21:40:01', 2, 1, NULL, 0),
(22, 'Da!', 'Ja!', 'assets/content/articles/3193.txt', '2015-01-11 21:40:19', 2, 1, NULL, 0),
(23, 'Papa''s poronga', 'Todo el mundo quiere probar la poronga del papa', 'assets/content/articles/5772.txt', '2015-01-11 21:41:27', 2, 1, NULL, 0),
(24, 'dda', 'dd', 'assets/content/articles/1272.txt', '2015-01-11 21:57:38', 2, 1, NULL, 0),
(25, 'Me meo', 'Jajajiaja.', 'assets/content/articles/9387.txt', '2015-01-11 21:57:59', 2, 1, NULL, 0),
(26, 'Aspotronkos', 'ada', 'assets/content/articles/9641.txt', '2015-01-11 21:58:41', 2, 1, NULL, 0),
(27, 'Aspotronkos es una mierda', 'Derp. Muchos derps. Toneladas de ellos.', 'assets/content/articles/5818.txt', '2015-01-11 22:59:33', 2, 1, NULL, 0),
(28, 'Y tú más', '¿Cómo era eso de Lorem ipsum?', 'assets/content/articles/3319.txt', '2015-01-11 23:01:13', 2, 1, NULL, 0),
(29, 'pedipálpolis', 'Mola.', 'assets/content/articles/1688.txt', '2015-01-11 23:02:45', 2, 1, NULL, 0),
(30, 'Naninonanino...', 'Hu-ha!!', 'assets/content/articles/7377.txt', '2015-01-11 23:04:01', 2, 1, NULL, 0),
(31, 'Son, I am derp.', 'Derp', 'assets/content/articles/1173.txt', '2015-01-11 23:07:09', 2, 1, NULL, 0),
(32, 'The Patriots!', 'LA-LI-LU-LE-LO', 'assets/content/articles/1313.txt', '2015-01-11 23:12:28', 2, 1, NULL, 0),
(33, 'd', 'd', 'assets/content/articles/3771.txt', '2015-01-11 23:12:49', 2, 1, NULL, 0),
(36, 'Welcome to Derp Land', 'Bienvenidos a Derp Land, un recinto de paz, amor y mucha subnormalidad.', 'assets/content/articles/3442.txt', '2015-01-11 23:29:18', 2, 1, 'assets/images/articles/1657.png', 0),
(40, 'tomate', 'lechuga', 'assets/content/articles/5346.txt', '2015-01-14 22:25:56', 4, 1, NULL, 0),
(41, 'Todo', 'Algo', 'assets/content/articles/5899.txt', '2015-01-15 02:38:20', 4, 1, NULL, 0),
(42, 'Vacía', 'vacía', 'assets/content/articles/8332.txt', '2015-01-15 02:38:53', 4, 1, NULL, 0),
(43, 'Probando avatar...', 'da', 'assets/content/articles/9515.txt', '2015-01-15 02:56:16', 4, 1, NULL, 0),
(44, 'Draft de prueba', 'Pruebo porque molo', 'assets/content/articles/5.txt', '2015-01-15 04:46:00', 2, 1, NULL, 1),
(45, 'Pana', 'd', 'assets/content/articles/3611.txt', '2015-01-15 05:00:34', 2, 1, NULL, 0),
(46, 'Será draft??', 'Pues eso.', 'assets/content/articles/8533.txt', '2015-01-15 05:00:49', 2, 1, NULL, 0),
(47, 'A ver ahora...', 'Si hacemos un puto draft', 'assets/content/articles/7979.txt', '2015-01-15 05:03:10', 2, 1, NULL, 0),
(48, 'q', 'q', 'assets/content/articles/2291.txt', '2015-01-15 05:03:53', 2, 1, NULL, 0),
(49, 'Hola', 'h', 'assets/content/articles/9489.txt', '2015-01-15 05:05:37', 2, 1, NULL, 0),
(50, 'y', 'y', 'assets/content/articles/9684.txt', '2015-01-15 05:11:07', 2, 1, NULL, 0),
(51, 'd', 'd', 'assets/content/articles/7153.txt', '2015-01-15 05:14:32', 2, 1, NULL, 1),
(52, 'dodo se presenta', 'hola a todos. HA LLEGADO DODO.', 'assets/content/articles/7747.txt', '2015-01-15 12:25:03', 5, 1, NULL, 1),
(53, 'dodododo', 'dodododo', 'assets/content/articles/6752.txt', '2015-01-15 13:28:03', 4, 1, NULL, 1),
(54, 't', 'y', 'assets/content/articles/4128.txt', '2015-01-15 13:32:55', 2, 1, NULL, 1),
(55, 'y', 'y', 'assets/content/articles/9271.txt', '2015-01-15 13:33:41', 2, 1, NULL, 1),
(56, 'pollo', 'pollo', 'assets/content/articles/4924.txt', '2015-01-15 13:34:31', 2, 1, NULL, 1),
(57, 'ee', 'e', 'assets/content/articles/6891.txt', '2015-01-15 13:37:09', 2, 1, NULL, 1),
(58, 'po', 'po', 'assets/content/articles/6515.txt', '2015-01-15 13:37:40', 2, 1, NULL, 1),
(59, 'dpdo', 'dodo', 'assets/content/articles/3499.txt', '2015-01-15 13:38:33', 2, 1, NULL, 1),
(60, 'y', 'y', 'assets/content/articles/7368.txt', '2015-01-15 13:40:36', 2, 1, NULL, 1),
(61, 'y', 'y', 'assets/content/articles/8755.txt', '2015-01-15 13:46:29', 2, 1, NULL, 0),
(62, 'yoyoyoy', 'yoyoyoo', 'assets/content/articles/9373.txt', '2015-01-15 13:46:47', 2, 1, NULL, 1);

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `email`, `password`, `admin`, `urlPic`, `alias`, `apellidos`, `sexo`, `fechaNac`) VALUES
(1, 'Pepe', 'pepe@gmail.com', '1234', 0, 'assets/images/users/pepe-avatar.png', 'pepe45', 'peponcio', 'hombre', NULL),
(2, 'Papadopoulos', 'papa_dopoulos@gmail.com', '1234', 0, 'assets/images/users/papadopoulos-avatar.png', 'papa_dopoulos', 'karagounis', 'hombre', NULL),
(3, 'Pollo', 'pollo@gmail.com', '1234', 0, 'assets/images/users/pollo-avatar.png', 'master_pollo', 'Pollastre', 'hombre', NULL),
(4, 'admin', 'admin@gmail.com', '1234', 1, 'assets/images/users/7921.png', 'admin', 'pollozs', 'mujer', NULL),
(5, 'dodo', 'dodo@gmail.com', '1234', 0, NULL, 'dodo', 'gonzalez', NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
