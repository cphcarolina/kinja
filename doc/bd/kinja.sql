-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-01-2015 a las 05:28:37
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `kinja`
--
CREATE DATABASE IF NOT EXISTS `kinja` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `kinja`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--
-- Creación: 07-01-2015 a las 18:00:59
--

DROP TABLE IF EXISTS `blogs`;
CREATE TABLE IF NOT EXISTS `blogs` (
`id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `urlPic` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `blog_padre` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`id`, `nombre`, `urlPic`, `descripcion`, `blog_padre`) VALUES
(1, 'Gawker', 'assets/images/blogs/gawker-logo.png', 'The definitive news and gossip sheet for followers of entertainment, media, and business.', NULL),
(2, 'Jalopnik', 'assets/images/blogs/jalopnik-logo.png', 'Blog de coches', NULL),
(3, 'Secundario1', NULL, 'Un blog de mala muerte donde no entra nadie', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs_usuario`
--
-- Creación: 10-01-2015 a las 16:48:01
--

DROP TABLE IF EXISTS `blogs_usuario`;
CREATE TABLE IF NOT EXISTS `blogs_usuario` (
  `autor` int(11) NOT NULL,
  `blog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `blogs_usuario`
--

INSERT INTO `blogs_usuario` (`autor`, `blog`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--
-- Creación: 10-01-2015 a las 00:21:31
--

DROP TABLE IF EXISTS `comentario`;
CREATE TABLE IF NOT EXISTS `comentario` (
`id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `urlContenido` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `autor` int(11) NOT NULL,
  `noticia` int(11) NOT NULL,
  `replyTo` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `comentario`
--

INSERT INTO `comentario` (`id`, `fecha`, `urlContenido`, `autor`, `noticia`, `replyTo`) VALUES
(1, '2015-01-08 19:33:23', 'assets/content/comments/1.txt', 2, 1, NULL),
(2, '2015-01-08 19:33:23', 'assets/content/comments/2.txt', 1, 3, NULL),
(4, '2015-01-08 23:53:25', 'assets/content/comments/3.txt', 3, 1, 1),
(5, '2015-01-10 00:40:17', 'assets/content/comments/4.txt', 3, 2, NULL),
(7, '2015-01-10 16:20:08', 'assets/content/comments/5.txt', 2, 2, 5),
(29, '2015-01-10 15:42:47', 'assets/content/comments/5.txt', 3, 3, NULL),
(30, '2015-01-10 15:43:11', 'assets/content/comments/5.txt', 3, 1, NULL),
(31, '2015-01-10 15:44:55', 'assets/content/comments/5.txt', 3, 1, NULL),
(36, '2015-01-10 16:30:20', 'assets/content/comments/5.txt', 2, 1, 30),
(37, '2015-01-10 16:30:55', 'assets/content/comments/5.txt', 2, 1, 4),
(38, '2015-01-10 16:33:00', 'assets/content/comments/5.txt', 2, 1, 31),
(39, '2015-01-10 17:41:40', 'assets/content/comments/5.txt', 1, 1, 38),
(57, '2015-01-10 22:32:22', 'assets/content/comments/863.txt', 2, 2, NULL),
(58, '2015-01-10 22:35:44', 'assets/content/comments/3128.txt', 2, 2, NULL),
(59, '2015-01-10 22:46:10', 'assets/content/comments/7514.txt', 2, 4, NULL),
(60, '2015-01-10 22:49:00', 'assets/content/comments/8498.txt', 2, 2, NULL),
(61, '2015-01-10 22:54:02', 'assets/content/comments/8479.txt', 2, 4, NULL),
(66, '2015-01-10 23:18:11', 'assets/content/comments/7416.txt', 2, 1, NULL),
(67, '2015-01-10 23:18:43', 'assets/content/comments/3287.txt', 2, 5, NULL),
(68, '2015-01-10 23:19:14', 'assets/content/comments/2174.txt', 3, 5, NULL),
(69, '2015-01-10 23:19:40', 'assets/content/comments/4354.txt', 3, 5, NULL),
(70, '2015-01-10 23:20:54', 'assets/content/comments/1981.txt', 3, 6, NULL),
(71, '2015-01-10 23:21:55', 'assets/content/comments/3976.txt', 3, 5, NULL),
(72, '2015-01-10 23:27:10', 'assets/content/comments/7416.txt', 3, 5, 67),
(73, '2015-01-10 23:28:06', 'assets/content/comments/3439.txt', 3, 1, 66),
(74, '2015-01-10 23:54:32', 'assets/content/comments/7274.txt', 3, 4, 59),
(75, '2015-01-11 00:13:32', 'assets/content/comments/7334.txt', 3, 4, 59),
(76, '2015-01-11 00:13:44', 'assets/content/comments/1694.txt', 3, 4, NULL),
(77, '2015-01-11 00:14:00', 'assets/content/comments/6186.txt', 3, 4, 76),
(78, '2015-01-11 00:53:10', 'assets/content/comments/8419.txt', 1, 7, NULL),
(79, '2015-01-11 01:53:31', 'assets/content/comments/2124.txt', 1, 14, NULL),
(80, '2015-01-11 02:00:35', 'assets/content/comments/8148.txt', 1, 14, NULL),
(81, '2015-01-11 02:01:10', 'assets/content/comments/2791.txt', 1, 3, NULL),
(82, '2015-01-11 02:01:23', 'assets/content/comments/9974.txt', 1, 3, 29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compartidas`
--
-- Creación: 08-01-2015 a las 15:30:17
--

DROP TABLE IF EXISTS `compartidas`;
CREATE TABLE IF NOT EXISTS `compartidas` (
`id` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `noticia` int(11) NOT NULL,
  `usuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--
-- Creación: 08-01-2015 a las 15:30:17
--

DROP TABLE IF EXISTS `noticias`;
CREATE TABLE IF NOT EXISTS `noticias` (
`id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `cabecera` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `urlContenido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `urlImagen` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `cabecera`, `urlContenido`, `fecha`, `autor`, `blog`, `urlImagen`) VALUES
(1, 'Ha muerto Franco', 'Ha muerto el bajito gordo con la voz más aguda de España.', 'assets/content/articles/1.txt', '2015-01-08 15:47:15', 1, 1, 'assets/images/articles/1.png'),
(2, 'Mariano Rajoy confirma que es homosexual', 'El Presidente del Gobierno de todos los españoles acaba de salir del armario', 'assets/content/articles/2.txt', '2015-01-08 15:47:15', 1, 1, 'assets/images/articles/2.png'),
(3, 'Ha salido a la venta un nuevo Citröen 2CV', 'Ahora cuenta con tecnología híbrida y tracción a las cuatro ruedas.', 'assets/content/articles/3.txt', '2015-01-08 18:32:42', 2, 2, 'assets/images/articles/3.png'),
(4, 'Desarticulada una red de tráfico de cepillos de dientes', 'La red robaba cepillos de dientes en comercios chinos y los revendía a la mitad de precio.', 'assets/content/articles/4.txt', '2015-01-08 23:14:43', 2, 1, 'assets/images/articles/4.png'),
(5, 'Noticia aleatoria', 'Nobody expects the Spanish Inquisition!', 'assets/content/articles/5.txt', '2015-01-09 13:49:28', 3, 1, NULL),
(6, 'Encontrado el huevo que perdió Franco en África', 'Una excavación reciente ha encontrado lo que parece ser los restos arqueológicos del huevo que le faltaba a Franco.', 'assets/content/articles/6.txt', '2015-01-09 22:45:45', 3, 3, NULL),
(7, '¿Qué es lorem ipsum?', 'Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, cuando un impresor (N. del T. persona dedicada a la imprenta) desconocido...', 'assets/content/articles/7.txt', '2015-01-11 00:48:41', 3, 1, 'assets/images/articles/7.png'),
(8, 'Otra noticia', 'Latin de la Universidad de Hampden-Sydney en Virginia, encontró una de las palabras más oscuras de la lengua del latín, "consecteur", en un pasaje de Lorem Ipsum, y al seguir leyendo distintos textos del latín, descubrió la fuente indudable.', 'assets/content/articles/8.txt', '2015-01-11 01:27:26', 1, 1, NULL),
(9, 'Porro', 'Porrompompero pero', 'assets/content/articles/9.txt', '2015-01-11 01:29:39', 1, 1, NULL),
(10, 'Una historia sobre el apareamiento del lince austral', 'Interesante de narices. Lorem ipsum... bla bla bla...', 'assets/content/articles/10.txt', '2015-01-11 01:38:38', 3, 1, 'assets/images/articles/10.png'),
(11, 'Noticia número 9', 'Pues eso.', 'assets/content/articles/11.txt', '2015-01-11 01:42:49', 2, 1, NULL),
(12, 'Lorem ipsum!!', 'Spam!!!', 'assets/content/articles/12.txt', '2015-01-11 01:44:22', 1, 1, NULL),
(13, 'Pedipalpos puestos a la venta en el mercado de Constantinopla', 'Lorem ipsum... y eso', 'assets/content/articles/13.txt', '2015-01-11 01:46:12', 2, 1, 'assets/images/articles/13.png'),
(14, 'Robert Pattinson es declarado el hombre más sexy del planeta', 'Y esta foto claramente lo demuestra.', 'assets/content/articles/14.txt', '2015-01-11 01:52:40', 2, 1, 'assets/images/articles/14.png'),
(15, 'Gallo claudio es asesinado', 'Ha sido asesinado al salir de su casa.', 'assets/content/articles/15.txt', '2015-01-11 03:02:18', 3, 1, 'assets/images/articles/15.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--
-- Creación: 07-01-2015 a las 17:59:02
--

DROP TABLE IF EXISTS `usuario`;
CREATE TABLE IF NOT EXISTS `usuario` (
`id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(12) COLLATE utf8_spanish_ci NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `urlPic` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombre`, `email`, `password`, `admin`, `urlPic`) VALUES
(1, 'Pepe', 'pepe@gmail.com', '1234', 0, 'assets/images/users/pepe-avatar.png'),
(2, 'Papadopoulos', 'papa_dopoulos@gmail.com', '1234', 0, 'assets/images/users/papadopoulos-avatar.png'),
(3, 'Pollo', 'pollo@gmail.com', '1234', 0, 'assets/images/users/pollo-avatar.png');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blogs`
--
ALTER TABLE `blogs`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `nombre` (`nombre`), ADD KEY `fk_blog` (`blog_padre`);

--
-- Indices de la tabla `blogs_usuario`
--
ALTER TABLE `blogs_usuario`
 ADD PRIMARY KEY (`autor`,`blog`), ADD KEY `blog` (`blog`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
 ADD PRIMARY KEY (`id`), ADD KEY `autor` (`autor`), ADD KEY `noticia` (`noticia`), ADD KEY `replyTo` (`replyTo`);

--
-- Indices de la tabla `compartidas`
--
ALTER TABLE `compartidas`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_noticia_compartidas` (`noticia`), ADD KEY `fk_blog_compartidas` (`blog`), ADD KEY `fk_usuario_compartidas` (`usuario`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
 ADD PRIMARY KEY (`id`), ADD KEY `fk_autor_noticias` (`autor`), ADD KEY `fk_blogs_noticias` (`blog`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blogs`
--
ALTER TABLE `blogs`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `comentario`
--
ALTER TABLE `comentario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=83;
--
-- AUTO_INCREMENT de la tabla `compartidas`
--
ALTER TABLE `compartidas`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `blogs`
--
ALTER TABLE `blogs`
ADD CONSTRAINT `fk_blog` FOREIGN KEY (`blog_padre`) REFERENCES `blogs` (`id`);

--
-- Filtros para la tabla `blogs_usuario`
--
ALTER TABLE `blogs_usuario`
ADD CONSTRAINT `blogs_usuario_ibfk_1` FOREIGN KEY (`autor`) REFERENCES `usuario` (`id`),
ADD CONSTRAINT `blogs_usuario_ibfk_2` FOREIGN KEY (`blog`) REFERENCES `blogs` (`id`);

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
ADD CONSTRAINT `comentario_ibfk_1` FOREIGN KEY (`autor`) REFERENCES `usuario` (`id`),
ADD CONSTRAINT `comentario_ibfk_2` FOREIGN KEY (`noticia`) REFERENCES `noticias` (`id`),
ADD CONSTRAINT `comentario_ibfk_3` FOREIGN KEY (`replyTo`) REFERENCES `comentario` (`id`);

--
-- Filtros para la tabla `compartidas`
--
ALTER TABLE `compartidas`
ADD CONSTRAINT `fk_blog_compartidas` FOREIGN KEY (`blog`) REFERENCES `blogs` (`id`),
ADD CONSTRAINT `fk_noticia_compartidas` FOREIGN KEY (`noticia`) REFERENCES `noticias` (`id`),
ADD CONSTRAINT `fk_usuario_compartidas` FOREIGN KEY (`usuario`) REFERENCES `usuario` (`id`);

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
ADD CONSTRAINT `fk_autor_noticias` FOREIGN KEY (`autor`) REFERENCES `usuario` (`id`),
ADD CONSTRAINT `fk_blogs_noticias` FOREIGN KEY (`blog`) REFERENCES `blogs` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
