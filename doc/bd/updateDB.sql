/*
 * Vaciar tabla comentario
 *  1. Elimina los comentarios a comentarios.
 *  2. Elimina los comentarios a noticias.
 */
DELETE FROM comentario
WHERE replyTo IS NOT NULL;
DELETE FROM comentario;
ALTER TABLE comentario AUTO_INCREMENT=1;


/*
 * Vacia tabla compartidas
 */
DELETE FROM compartidas;
ALTER TABLE compartidas AUTO_INCREMENT=1;

/*
 * Vacia tabla noticias
 */
DELETE FROM noticias;
ALTER TABLE noticias AUTO_INCREMENT=1;

/*
 * Vacia tabla blogs_usuario
 */
DELETE FROM blogs_usuario;
ALTER TABLE blogs_usuario AUTO_INCREMENT=1;
 
/*
 * Vacia tabla usuario
 */
DELETE FROM usuario;
ALTER TABLE usuario AUTO_INCREMENT=1;
 
/*
 * Vacia tabla blogs
 *  1. Elimina los blogs con padre.
 *  2. Elimina los blogs principales.
 */
DELETE FROM blogs
WHERE blog_padre IS NOT NULL;
DELETE FROM blogs;
ALTER TABLE blogs AUTO_INCREMENT=1;


/*
 * Crear tabla comentario
 */
DELETE FROM comentario
WHERE replyTo IS NOT NULL;
DELETE FROM comentario;
ALTER TABLE comentario AUTO_INCREMENT=1;


/*
 * Crear tabla blogs
 */

 CREATE TABLE IF NOT EXISTS `blogs` (
`id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `urlPic` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `blog_padre` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
 -----


/*
* Crear tabla usuario
*/

/*
* Crear tabla blogs_usuario
*/
CREATE TABLE IF NOT EXISTS `blogs_usuario` (
  `autor` int(11) NOT NULL,
  `blog` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
.-----

/*
 * Crear tabla noticias
 */
CREATE TABLE IF NOT EXISTS `noticias` (
`id` int(11) NOT NULL,
  `titulo` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cabecera` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `urlContenido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `autor` int(11) NOT NULL,
  `blog` int(11) NOT NULL,
  `urlImagen` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;


/*
 * Crear compartidas
 */




/*
 * Crear tabla comentarios
 */
CREATE TABLE IF NOT EXISTS `comentario` (
`id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `urlContenido` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `autor` int(11) NOT NULL,
  `noticia` int(11) NOT NULL,
  `replyTo` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;
----
CREATE TABLE IF NOT EXISTS `comentario` (
`id` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `urlContenido` varchar(250) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `autor` int(11) NOT NULL,
  `noticia` int(11) NOT NULL,
  `replyTo` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;


 



/*
 * Realizar los cambios estructurales que sean precisos
 */
ALTER TABLE `noticias` CHANGE `titulo` `titulo` VARCHAR(100);
ALTER TABLE `usuario` CHANGE `nombre` `alias` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `usuario` ADD `nombre` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `usuario` ADD `apellidos` VARCHAR(150) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL;
ALTER TABLE `usuario` ADD `fechaNac` DATE NULL;
ALTER TABLE `usuario` ADD `sexo` SET('hombre','mujer') CHARACTER SET utf8 COLLATE utf8_spanish_ci NULL ;

/*
 * Insertar los nuevos valores
 * de forma ordenada.
 */

/* blogs */
INSERT INTO `blogs` (`id`, `nombre`, `urlPic`, `descripcion`, `blog_padre`) VALUES
(1, 'Gawker', 'assets/images/blogs/gawker-logo.png', 'The definitive news and gossip sheet for followers of entertainment, media, and business.', NULL),
(2, 'Jalopnik', 'assets/images/blogs/jalopnik-logo.png', 'Blog de coches', NULL),
(3, 'Secundario1', NULL, 'Un blog de mala muerte donde no entra nadie', 1);
---

INSERT INTO `blogs` (`id`, `nombre`, `urlPic`, `descripcion`, `blog_padre`) VALUES
(1, 'Gawker', 'assets/images/blogs/gawker-logo.png', 'The definitive news and gossip sheet for followers of entertainment, media, and business.', NULL),
(2, 'Jalopnik', 'assets/images/blogs/jalopnik-logo.png', 'Blog de coches', NULL),
(3, 'Secundario1', NULL, 'Un blog de mala muerte donde no entra nadie', 1);
---

/* usuarios */
INSERT INTO `usuario` (`id`, `alias`, `email`, `password`, `admin`, `urlPic`, `nombre`, `apellidos`, `fechaNac`, `sexo`) VALUES
(1, 'Pepe', 'pepe@gmail.com', '1234', 0, 'assets/images/users/pepe-avatar.png', 'Pepe', 'Palomo', NULL, 'hombre'),
(2, 'Papadopoulos', 'papa_dopoulos@gmail.com', '1234', 0, NULL, 'María', 'Esteban', NULL, 'mujer'),
(3, 'Pollo', 'pollo@gmail.com', '1234', 0, 'assets/images/users/pollo-avatar.png', 'Pollo', 'Pollito', NULL, NULL),
(4, 'carolina', 'cphcarolina@gmail.com', 'qwe', 0, NULL, 'Carolina', 'Prada', '1987-10-06', 'mujer');

/* blogs_usuarios */

INSERT INTO `blogs_usuario` (`autor`, `blog`) VALUES
(1, 3);
---
/* noticias */
INSERT INTO `noticias` (`id`, `titulo`, `cabecera`, `urlContenido`, `fecha`, `autor`, `blog`, `urlImagen`) VALUES
(1, 'Ha muerto Franco', 'Ha muerto el bajito gordo con la voz más aguda de España.', 'assets/content/articles/1.txt', '2015-01-08 15:47:15', 1, 1, 'assets/images/articles/1.png'),
(2, 'Mariano Rajoy confirma que es homosexual', 'El Presidente del Gobierno de todos los españoles acaba de salir del armario', 'assets/content/articles/2.txt', '2015-01-08 15:47:15', 1, 1, 'assets/images/articles/2.png'),
(3, 'Ha salido a la venta un nuevo Citröen 2CV', 'Ahora cuenta con tecnología híbrida y tracción a las cuatro ruedas.', 'assets/content/articles/3.txt', '2015-01-08 18:32:42', 2, 2, 'assets/images/articles/3.png'),
(4, 'Desarticulada una red de tráfico de cepillos de dientes', 'La red robaba cepillos de dientes en comercios chinos y los revendía a la mitad de precio.', 'assets/content/articles/4.txt', '2015-01-08 23:14:43', 2, 1, 'assets/images/articles/4.png'),
(5, 'Noticia aleatoria', 'Nobody expects the Spanish Inquisition!', 'assets/content/articles/5.txt', '2015-01-09 13:49:28', 3, 1, NULL),
(6, 'Encontrado el huevo que perdió Franco en África', 'Una excavación reciente ha encontrado lo que parece ser los restos arqueológicos del huevo que le faltaba a Franco.', 'assets/content/articles/6.txt', '2015-01-09 22:45:45', 3, 3, NULL);

/* compartidas */
/* comentarios */
INSERT INTO `comentario` (`id`, `fecha`, `urlContenido`, `autor`, `noticia`, `replyTo`) VALUES
(1, '2015-01-08 19:33:23', 'assets/content/comments/1.txt', 2, 1, NULL),
(2, '2015-01-08 19:33:23', 'assets/content/comments/2.txt', 1, 3, NULL),
(4, '2015-01-08 23:53:25', 'assets/content/comments/3.txt', 3, 1, 1),
(5, '2015-01-10 00:40:17', 'assets/content/comments/4.txt', 3, 2, NULL),
(6, '2015-01-10 02:08:13', 'assets/content/comments/5.txt', 3, 6, NULL),
(7, '2015-01-10 02:30:51', 'assets/content/comments/5.txt', 1, 5, NULL);