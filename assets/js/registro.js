$(document).ready(function() {
	$("#formulario").submit(function(e) {
	
		if ($("#pass1").val()!==$("#pass2").val()) {
			$("#errorPass").html("<div class='alert alert-danger'>Las contraseñas deben coincidir.</div>");
			$("#groupPass1").addClass("has-error");
			$("#pass1").val("")
			$("#pass2").val("")
			$("#groupPass2").addClass("has-error");
			$("#pass1").focus();
			e.preventDefault();
		}
		var email = $("#email").val();
		if((email.indexOf("@")==-1) || (email.indexOf(".")==-1) || (email.indexOf("@") > email.indexOf(".")) || (email.indexOf(".es")==-1 && email.indexOf(".com")==-1)) {
			$("#errorEmail").html("<div class='alert alert-danger'>Introduzca una dirección de correo válida (¿olvidó el proveedor de cuentas de correo?).</div>");
			$("#groupEmail").addClass("has-error");
			$("#email").focus();
			e.preventDefault();
		}
		if($("#fechaNac").val()!="") {
			var aux = $("#fechaNac").val().charAt(2);
			if(aux=='/') {
				var dia = $("#fechaNac").val().slice(0,2);
				var mes = $("#fechaNac").val().slice(3,5);
				var anyo = $("#fechaNac").val().slice(6);
			}
			else {
				var dia = $("#fechaNac").val().slice(8,10);
				var mes = $("#fechaNac").val().slice(5,7);
				var anyo = $("#fechaNac").val().slice(0,4);				
			}
			var fecha = new Date(mes+"/"+dia+"/"+anyo);
			if(fecha == "Invalid Date") {
				$("#errorFecha").html("<div class='alert alert-danger'>Introduzca una fecha válida (DD/MM/YYYY o YYYY/MM/DD).</div>");
				$("#groupFecha").addClass("has-error");
				$("#fechaNac").focus();
				e.preventDefault();
			}
		}
		
		// Comprobar el formato de la fecha
	});

	$("#pass2").change(function() {
		if ($("#pass1").val()===$("#pass2").val()) {
			$("#errorPass").html("");
			$("#groupPass1").removeClass("has-error");
			$("#groupPass2").removeClass("has-error");
		}
	});
	
	$("#email").change(function() {
		var email = $("#email").val();
		if(!((email.indexOf("@")==-1) || (email.indexOf(".")==-1) || (email.indexOf("@") > email.indexOf(".")) || (email.indexOf(".es")==-1 && email.indexOf(".com")==-1))) {				
			$("#errorEmail").html("");
			$("#groupEmail").removeClass("has-error");				
		}	
	});
	$("#fechaNac").change(function() {
		var correcto = false;
		if($("#fechaNac").val=="") {
			correcto = true;
		}
		else {
			var aux = $("#fechaNac").val().charAt(2);
			if(aux=='/') {
				var dia = $("#fechaNac").val().slice(0,2);
				var mes = $("#fechaNac").val().slice(3,5);
				var anyo = $("#fechaNac").val().slice(6);
			}
			else {
				var dia = $("#fechaNac").val().slice(8,10);
				var mes = $("#fechaNac").val().slice(5,7);
				var anyo = $("#fechaNac").val().slice(0,4);				
			}
			var fecha = new Date(mes+"/"+dia+"/"+anyo);
			if(!(fecha=="Invalid Date")) {
				correcto = true;
			}
		}
		if(correcto==true) {
			$("#errorFecha").html("");
			$("#groupFecha").removeClass("has-error");	
		}
	});
});