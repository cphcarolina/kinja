// autocomplet : this function will be executed every time we change the text
/*function autocomplet() {
    var min_length = 0; // min caracters to display the autocomplete

    var keyword = $('#usuario').val();
    if (keyword.length >= min_length) {
        $.ajax({
            url: 'ajax_refresh.php',
            type: 'POST',
            data: {keyword:keyword},
            success:function(data){
                $('#listaUsuarios').show();
                $('#listaUsuarios').html(data);
            }
        });
    } else {
        $('#listaUsuarios').hide();
    }
}
 
// set_item : this function will be executed when we select an item
function set_item(item) {
    // change input value
    $('#usuario').val(item);
    // hide proposition list
    $('#listaUsuarios').hide();
}

*/

function lookup(inputString) {
    $('#alert').hide();
    if(inputString.length == 0) {
        $('#listaUsuarios').hide();
    } else {
        $.post("<?php //echo base_url(); ?>"+"/admin/get_usuarios/", {queryString: ""+inputString+""}, function(data){
            if(data.length > 0) {
                $('#listaUsuarios').show();
                $('#listaUsuarios').html(data);
            }
        });
    }
}

function fill(thisValue) {
    $('#usuario').val(thisValue);
    setTimeout("$('#listaUsuarios').hide();", 200);
}
/*

$(function(){
  $("#usuario").autocomplete({
    source: "admin/get_usuarios" // path to the get_birds method
  });
});

*/