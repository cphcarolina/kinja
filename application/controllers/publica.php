<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Publica extends CI_Controller {
	private $data = array();

	function __construct() {
		parent::__construct();

		$this->load->model('Blog_m', '', TRUE);
		$this->load->model('Noticia_m', '', TRUE);
		$this->load->model('Usuario_m', '', TRUE);
		$this->load->model('Comentario_m', '', TRUE);
		$this->load->helper('ckeditor');

		// Cargar datos de cabecera
		$this->data['mainB'] = $this->Blog_m->get_main_blogs();

		// Cargar la sesión
		$user = $this->session->userdata('userdata');
		if($user) {
			$this->data['userdata'] = $user;
			$this->data['userB'] = $this->Usuario_m->get_blogs($user->id);
		}
	} 

	public function index()
	{
		$this->load->view('publica/index', $this->data);
	}

	public function blog($id='1', $pag='0')
	{
		if($this->Blog_m->is_main($id)) {
			$this->data['otherB'] = $this->Blog_m->get_list_featured($id);
		}
		$this->data['pag'] = $pag;
		$this->data['total_articles'] = $this->Blog_m->get_total_articles($id,$pag)->total;
		$this->data['blog'] = $this->Blog_m->get_byId($id);
		$this->data['ultimas'] = $this->Blog_m->get_ultimasnoticias($id, $pag);
		$this->data['popular'] = $this->Blog_m->get_popular($id);
		$this->data['trending'] = $this->Blog_m->get_trending();
		$this->data['trending_blog'] = $this->Blog_m->get_blog_trending();
		$this->data['ultimas_usuario'] = $this->Blog_m->get_user_ultimas($id, $pag);
		$this->data['ultimas_comments'] = $this->Blog_m->get_ultimascomments($id, $pag);
		$this->data['popular_usuario'] = $this->Blog_m->get_user_popular($id);
		$this->data['more'] = $this->Blog_m->get_more($id, $pag+1);
		$this->load->view('publica/blog', $this->data);
	}

	public function usuario($id='1',$pag='0')
	{
		$this->data['pag'] = $pag;
		$this->data['usuario'] = $this->Usuario_m->get_byId($id);
		$this->data['total_articles'] = $this->Usuario_m->get_total_articles($id,$pag)->total;
		$this->data['ultimas'] = $this->Usuario_m->get_ultimasnoticias($id,$pag);
		$this->data['ultimas_comments'] = $this->Usuario_m->get_ultimascomments($id,$pag);
		$this->data['popular'] = $this->Usuario_m->get_popular($id);
		$this->data['more'] = $this->Usuario_m->get_more($id,$pag+1);
		$this->load->view('publica/usuario', $this->data);
	}


	public function noticia($id='1')
	{
		$this->data['noticia'] = $this->Noticia_m->get_byId($id);
		$id_blog = $this->Noticia_m->get_byId($id)->blog;
		$id_autor = $this->Noticia_m->get_byId($id)->autor;
		$this->data['blog'] = $this->Blog_m->get_byId($id_blog);
		$this->data['autor'] = $this->Usuario_m->get_byId($id_autor);
		$this->data['comments'] = $this->Noticia_m->get_comments($id);
		$this->data['usuario_comments'] = $this->Noticia_m->get_user_comments($id);
		$this->data['trending'] = $this->Blog_m->get_trending();
		$this->data['trending_blog'] = $this->Blog_m->get_blog_trending();
		$this->data['writtenBy'] = $this->Usuario_m->get_writtenBy($id_autor);
		$this->data['total_comments'] = $this->Noticia_m->get_total_comments($id);
		$this->data['total_replies'] = $this->Noticia_m->get_total_replies($id);
		$this->load->view('publica/noticia', $this->data);
	}

	public function show_template()
	{
		$this->data['blog'] = $this->Blog_m->get_byId(1);
		$this->load->view('template');
	}

	public function escribir_comentario($id='1')
	{
		$this->data['noticia'] = $this->Noticia_m->get_byId($id);
		$id_blog = $this->Noticia_m->get_byId($id)->blog;
		$id_autor = $this->Noticia_m->get_byId($id)->autor;
		$this->data['blog'] = $this->Blog_m->get_byId($id_blog);
		$this->data['autor'] = $this->Usuario_m->get_byId($id_autor);
		$this->data['comments'] = $this->Noticia_m->get_comments($id);
		$this->data['usuario_comments'] = $this->Noticia_m->get_user_comments($id);
		$this->data['trending'] = $this->Blog_m->get_trending();
		$this->data['trending_blog'] = $this->Blog_m->get_blog_trending();
		$this->data['writtenBy'] = $this->Usuario_m->get_writtenBy($id_autor);
		$this->data['total_comments'] = $this->Noticia_m->get_total_comments($id);
		$this->data['total_replies'] = $this->Noticia_m->get_total_replies($id);
		//Ckeditor's configuration
		$this->data['ckeditor_comment'] = array(
 
			//ID of the textarea that will be replaced
			'id' 	=> 	'content',
			'path'	=>	'assets/js/ckeditor',
 
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Basic", 	//Using the Full toolbar
				'width' 	=> 	"600px",	//Setting a custom width
				'height' 	=> 	'200px',	//Setting a custom height
 
			),
		);
		$this->load->view('publica/noticia', $this->data);
	}

	//Envía un comentario a una noticia
	public function enviar_comentario($id='1')
	{
		if(isset($this->data['userdata'])) {
			$texto = $_POST ['content'];
			$a = 'a';
			$filename = 'assets/content/comments/'.rand(1,9).rand(1,9).rand(1,9).rand(1,9).".txt";
			file_put_contents($filename, $texto);
			$this->Noticia_m->new_comment($id, $this->data['userdata'], $filename);
		}
		$this->noticia($id);
	}

	//Ojo!! La id es del comentario
	public function escribir_respuesta($id='1')
	{
		$comentario = $this->Comentario_m->get_byId($id);
		$id_noticia = $comentario->noticia;
		$this->data['replyTo'] = $comentario;
		$this->data['noticia'] = $this->Noticia_m->get_byId($id_noticia);
		$id_blog = $this->Noticia_m->get_byId($id_noticia)->blog;
		$id_autor = $this->Noticia_m->get_byId($id_noticia)->autor;
		$this->data['blog'] = $this->Blog_m->get_byId($id_blog);
		$this->data['autor'] = $this->Usuario_m->get_byId($id_autor);
		$this->data['comments'] = $this->Noticia_m->get_comments($id_noticia);
		$this->data['usuario_comments'] = $this->Noticia_m->get_user_comments($id_noticia);
		$this->data['trending'] = $this->Blog_m->get_trending();
		$this->data['trending_blog'] = $this->Blog_m->get_blog_trending();
		$this->data['writtenBy'] = $this->Usuario_m->get_writtenBy($id_autor);
		$this->data['total_comments'] = $this->Noticia_m->get_total_comments($id_noticia);
		$this->data['total_replies'] = $this->Noticia_m->get_total_replies($id_noticia);
		//Ckeditor's configuration
		$this->data['ckeditor_reply'] = array(
 
			//ID of the textarea that will be replaced
			'id' 	=> 	'content',
			'path'	=>	'assets/js/ckeditor',
 
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Basic", 	//Using the Full toolbar
				'width' 	=> 	"400px",	//Setting a custom width
				'height' 	=> 	'170px',	//Setting a custom height
 
			),
		);
		$this->load->view('publica/noticia', $this->data);
	}

	//Envía una respuesta a un comentario (id = comentario)
	public function enviar_respuesta($id='1')
	{
		$comentario = $this->Comentario_m->get_byId($id);
		if(isset($this->data['userdata'])) {
			$texto = $_POST ['content'];
			$filename = 'assets/content/comments/'.rand(1,9).rand(1,9).rand(1,9).rand(1,9).".txt";
			file_put_contents($filename, $texto);
			$this->Noticia_m->new_reply($comentario, $this->data['userdata'], $filename);
		}
		$this->noticia($comentario->noticia);
	}
}
