<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Privada extends CI_Controller {
	private $data = array();

	function __construct() {
		parent::__construct();

		$this->load->model('Blog_m', '', TRUE);
		$this->load->model('Noticia_m', '', TRUE);
		$this->load->model('Usuario_m', '', TRUE);
		$this->load->model('Comentario_m', '', TRUE);
		$this->load->model('Usuario_m', '', TRUE);
		$this->load->helper('ckeditor');
		$this->load->helper('simpleimage');


		// Cargar datos de cabecera
		$this->data['mainB'] = $this->Blog_m->get_main_blogs();

		// Cargar la sesión
		$user = $this->session->userdata('userdata');
		if($user) {
			$this->data['userdata'] = $user;
			$this->data['userB'] = $this->Usuario_m->get_blogs($user->id);
		}
		else {
			$this->data['listado'] = $this->Blog_m->get_main_blogs();
			$this->data['error'] = "No tiene acceso a esta zona.";
			$this->load->view('publica/index', $this->data);
		}
	} 

	// Carga el perfil editable del usuario
	public function form_perfil() {
		if($this->data['userdata']) {
			$this->load->view('privada/perfil', $this->data);
		}
		else{
			$this->data['WTF'] = "algo";
			$this->load->view('privada/perfil', $this->data);
		}
	}

	// Carga el editor del blog personal
	public function form_blog() {
		if($this->data['userdata']) {
			$this->data['currentPage'] = 'mngBlog';
			$this->data['userBlog'] = $this->Blog_m->get_byAutor($this->data['userdata']->id);
			$this->load->view('privada/mngBlog', $this->data);
		}
	}

	// Carga el formulario para crear un nuevo post
	public function form_post() {
		if($this->data['userdata']) {
			$this->data['currentPage'] = 'newPost';
			$this->data['ckeditor_newPost'] = array(
 
			//ID of the textarea that will be replaced
			'id' 	=> 	'content',
			'path'	=>	'assets/js/ckeditor',
 
			//Optionnal values
			'config' => array(
				'toolbar' 	=> 	"Basic", 	//Using the Full toolbar
				'width' 	=> 	"600px",	//Setting a custom width
				'height' 	=> 	'200px',	//Setting a custom height
 
			),
		);
			$this->load->view('privada/newPost', $this->data);
		}
	}

	// Muestra el listado de los posts del usuario
	public function show_posts($pag='0') {
		if($this->data['userdata']) {
			$this->data['currentPage'] = 'posts';

			$id = $this->data['userdata']->id;
			$this->data['pag'] = $pag;
			$this->data['usuario'] = $this->Usuario_m->get_byId($id);
			$this->data['total_articles'] = $this->Usuario_m->get_total_articles($id,$pag)->total;
			$this->data['ultimas'] = $this->Usuario_m->get_ultimasnoticias($id,$pag);
			$this->data['ultimas_comments'] = $this->Usuario_m->get_ultimascomments($id,$pag);

			$this->data['more'] = $this->Usuario_m->get_more($id, $pag+1);
			$this->load->view('privada/listadoPosts', $this->data);
		}
	}

	// Muestra el listado de los comentarios del usuario
	public function show_comments($pag='0') {
		if($this->data['userdata']) {
			$this->data['currentPage'] = 'comments';

			$id = $this->data['userdata']->id;
			$this->data['pag'] = $pag;
			$this->data['total_user_comments'] = $this->Usuario_m->get_total_comments($id, $pag)->total;
			$this->data['usuario'] = $this->Usuario_m->get_byId($id);
			$this->data['comments'] = $this->Usuario_m->get_last_comments($id, $pag);

			$this->load->view('privada/listadoComments', $this->data);
		}
	}

	// Muestra el listado de los borradores del usuario
	public function show_Drafts() {
		if($this->data['userdata']) {
			$this->data['currentPage'] = 'drafts';

			$id = $this->data['userdata']->id;
			$this->data['drafts'] = $this->Usuario_m->get_drafts($id);
			$this->load->view('privada/listadoDrafts', $this->data);
		}
	}

	// Muestra el listado de las notificaciones del usuario
	public function show_notifications() {
		if($this->data['userdata']) {
			$this->data['currentPage'] = 'notif';
			$this->load->view('privada/listadoNotificaciones', $this->data);
		}
	}

	private function setSession() {
		$user = $this->Usuario_m->get_byId($this->data['userdata']->id);
		if($user) {
			$this->session->unset_userdata('userdata');
			$this->session->set_userdata('userdata', $user);
			$this->data['userdata'] = $user;
			$this->data['actualizado'] = "¡Sus datos se han actualizado correctamente!";
		}
	}

	/**
	 * EDITAR USER
	 */

	// Edita los datos del usuario
	public function edit_InfUser() {
		$alias = $this->input->post('alias');
		$email = $this->input->post('email');
		$pass1 = $this->input->post('pass1');
		$pass2 = $this->input->post('pass2');
		$pass3 = $this->input->post('pass3');

		if(strcmp($pass1,$pass2)==0) {
			$array = array(
				'alias' => $alias,
			);

			$actualizado = $this->Usuario_m->update_user($this->data['userdata']->id,$array);
			if($actualizado) {
				$this->setSession();
				$this->data['currentTab'] = 'infUser';
				$this->load->view('privada/perfil', $this->data);
			}
			else {
				$this->data['heading'] = "Error 500.";
				$this->data['message'] = "Se ha producido un error en el servidor. Inténtelo más tarde.";
				$this->load->view('errors/error_500', $this->data);
			}
		}
		else {
			$error = "Las contraseñas no son las mismas.";
		}
	}

	// Edita el avatar del usuario
	public function edit_Avatar() {
		if(isset($_FILES['avatar']) && ($_FILES['avatar']['type'] == "image/jpeg" || $_FILES['avatar']['type'] == "image/png") ) {
			
			$urlPic = 'assets/images/users/'.rand(1,9).rand(1,9).rand(1,9).rand(1,9).'.png';
			$image = new SimpleImage();
			$image->load($urlPic);
			if($image->getWidth() > 125) {
				$image->resizeToWidth(125); 			
			}
			if($image->getHeight() > 125) $image->resizeToHeight(125); 
			$image->save($urlPic);
			move_uploaded_file($_FILES['avatar']['tmp_name'], $urlPic);
			$array = array(
				'urlPic' => $urlPic
			);

			$actualizado = $this->Usuario_m->update_user($this->data['userdata']->id,$array);
			if($actualizado) {
				$this->setSession();
				$this->data['currentTab'] = 'avatar';
				$this->load->view('privada/perfil', $this->data);
			}
			else {
				$this->data['heading'] = "Error 500.";
				$this->data['message'] = "Se ha producido un error en el servidor. Inténtelo más tarde.";
				$this->load->view('errors/error_500', $this->data);
			}		
		}
		else {
			$this->data['error'] = "La imagen no es la adecuada, inténtelo con otro formato (jpeg/png)";
			$this->data['currentTab'] = 'avatar';
			$this->load->view('privada/perfil', $this->data);
		}
	}

	// Edita los datos personales del usuario
	public function edit_InfPersonal() {
		$nombre = $this->input->post('nombre');
		$apellidos = $this->input->post('apellidos');
		$fechaNac = $this->input->post('fechaNac');
		$sexo = $this->input->post('sexo');

		$array = array(
			'nombre' => $nombre,
			'apellidos' => $apellidos
		);

		$actualizado = $this->Usuario_m->update_user($this->data['userdata']->id,$array);
		if($actualizado) {
			$this->setSession();
			$this->data['currentTab'] = 'infPersonal';
			$this->load->view('privada/perfil', $this->data);
		}
		else {
			$this->data['heading'] = "Error 500.";
			$this->data['message'] = "Se ha producido un error en el servidor. Inténtelo más tarde.";
			$this->load->view('errors/error_500', $this->data);
		}
	}

	/**
	 *	EDITAR BLOG PERSONAL
	 */

	// Edita los datos del blog personal del usuario
	public function edit_InfBlog() {
		$blog = $this->Blog_m->get_byAutor($this->data['userdata']->id);
		$nombre = $this->input->post('nombre');
		$desc = $this->input->post('descripcion');

		$array = array(
			'nombre' => $nombre,
			'descripcion' => $desc
		);
		if(isset($this->data['userB'])) {
			$actualizado = $this->Blog_m->update_blog($blog->id,$array);
			if($actualizado) {
				$this->data['userB'] = $this->Blog_m->get_byId($blog->id);
				$this->load->view('privada/mngBlog', $this->data);
			}
			else {
				$this->data['heading'] = "Error 500.";
				$this->data['message'] = "Se ha producido un error en el servidor. Inténtelo más tarde.";
				$this->load->view('errors/error_500', $this->data);
			}
		}
		else {
			$creado = $this->Blog_m->create_blog($blog->id,$this->data['userdata'], $array);
			if($creado) {
				$this->data['userB'] = $this->Blog_m->get_byId($blog->id);
				$this->load->view('privada/mngBlog', $this->data);
			}
			else {
				$this->data['heading'] = "Error 500.";
				$this->data['message'] = "Se ha producido un error en el servidor. Inténtelo más tarde.";
				$this->load->view('errors/error_500', $this->data);
			}
		}


	}

	// Edita el logo del blog personal del usuario
	public function edit_Logo() {

	}


	// Envía una noticia creada en el formulario de Create Post
	public function enviar_noticia() {
		if(isset($this->data['userdata'])) {
			if ($_POST['action'] == "Publicar") {
				$draft = 0;
			}
			else {
				$draft = 1;	
			} 
			$texto = $_POST ['content'];
			$titulo = $_POST['titulo'];
			$cabecera = $_POST ['cabecera'];
			if(isset($_FILES['imagen']) && ($_FILES['imagen']['type'] == "image/jpeg" || $_FILES['imagen']['type'] == "image/png") ) {
				$urlImagen = 'assets/images/articles/'.rand(1,9).rand(1,9).rand(1,9).rand(1,9).'.png';
				$image = new SimpleImage();
				$image->load($urlImagen);
				if($image->getWidth() > 600) {
					$image->resizeToWidth(600); 			
				}
				if($image->getHeight() > 300) $image->resizeToHeight(300); 
				$image->save($urlImagen);
				move_uploaded_file($_FILES['imagen']['tmp_name'], $urlImagen);
			}
			else {
				$urlImagen = null;
			}

								
			$blog = 1; //CAMBIAR
			$filename = 'assets/content/articles/'.rand(1,9).rand(1,9).rand(1,9).rand(1,9).".txt";
			file_put_contents($filename, $texto);
			$this->Noticia_m->new_article($blog, $this->data['userdata'],$titulo, $cabecera, $urlImagen, $filename, $draft);
		}
		if ($_POST['action'] == "Publicar") {
				$this->show_posts();
			}
		else $this->show_drafts();


	}


}
