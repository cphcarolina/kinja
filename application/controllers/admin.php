<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
	private $data = array();

	function __construct() {
		parent::__construct();

		$this->load->model('Blog_m', '', TRUE);
		$this->load->model('Usuario_m', '', TRUE);
		$this->load->library('table');

		// Cargar los blogs principales
		$this->data['mainB'] = $this->Blog_m->get_main_blogs();

		// Cargar la sesión
		$user = $this->session->userdata('userdata');
		if($user && ($user->admin == 1)) {
			$this->data['userdata'] = $user;
			$this->data['userB'] = $this->Usuario_m->get_blogs($user->id);
		}
		else {
			$this->data['listado'] = $this->Blog_m->get_main_blogs();
			$this->data['error'] = "No tiene acceso a esta zona.";
			$this->load->view('publica/index', $this->data);
		}
	}

	// Carga el perfil editable del usuario
	public function index() {
		if($this->data['userdata'] && $this->data['userdata']->admin==1) {
			$this->load->view('backOffice/index', $this->data);
		}
	}

	// Carga el listado de blogs relacionados con el blog $id
	public function listado($id) {
		if($this->data['userdata'] && $this->data['userdata']->admin==1) {
			if($this->Blog_m->is_main($id)) {
				$this->data['otherB'] = $this->Blog_m->get_list_featured($id);
			}
			$this->data['padre'] = $this->Blog_m->get_byId($id);
			$this->load->view('backOffice/listBlogs', $this->data);
		}
	}

	// Carga el formulario para crear nuevos blogs
	public function form_newBlog() {
		if($this->data['userdata'] && $this->data['userdata']->admin==1) {
			$this->load->view('backOffice/newBlog', $this->data);
		}
	}

	// Carga el formulario manage Blog pro
	public function form_mngBlogPro($id) {
		if($this->data['userdata'] && $this->data['userdata']->admin==1) {

			$usuarios = $this->Blog_m->get_privilegiados($id);
			$this->data['blogdata'] = $this->Blog_m->get_byId($id);
			$this->data['usuarios'] = $usuarios;
			$this->load->view('backOffice/mngBlogPro', $this->data);
		}
	}

	public function get_usuarios(){
		$usuario = $this->input->get('usuario');
		if(isset($usuario)) {
			$q = strtolower($usuario);
			$this->Usuario_m->get_nombres($q);
		}
	}

	

	// Edita el avatar del usuario
	public function edit_Logo() {
		if(isset($_FILES['avatar']) && (
			$_FILES['avatar']['type'] == "image/jpeg" || 
			$_FILES['avatar']['type'] == "image/png")) {
			$urlPic = 'assets/images/blogs/'.rand(1,9).rand(1,9).rand(1,9).rand(1,9).'.png';
			$image = new SimpleImage();
			$image->load($urlPic);
			if($image->getWidth() > 125) {
				$image->resizeToWidth(125); 			
			}

			if($image->getHeight() > 125) $image->resizeToHeight(125); 
			$image->save($urlPic);
			move_uploaded_file($_FILES['avatar']['tmp_name'], $urlPic);
			$array = array(
				'urlPic' => $urlPic
			);

			$actualizado = $this->Blog_m->update_blog($this->data['blogdata']->id,$array);
			if($actualizado) {
				$this->data['blogdata'] = $actualizado;
				$this->data['currentTab'] = 'logo';
				$this->load->view('backOffice/mngBlogPro', $this->data);
			}
			else {
				$this->data['error'] = "Se ha producido un error en el servidor. Inténtelo más tarde.";
				$this->data['blogdata'] = $this->Blog_m->get_byId($id);
				$this->data['currentTab'] = 'logo';
				$this->load->view('backOffice/mngBlogPro', $this->data);
			}		
		}
		else {
			$this->data['error'] = "La imagen no es la adecuada, inténtelo con otro formato (jpeg/png)";
			$this->data['blogdata'] = $this->Blog_m->get_byId($id);
			$this->data['currentTab'] = 'logo';
			$this->load->view('backOffice/mngBlogPro', $this->data);
		}
	}
}