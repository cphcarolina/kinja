<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {
	private $data = array();

	function __construct() {
		parent::__construct();

		$this->load->model('Blog_m', '', TRUE);
		$this->load->model('Usuario_m', '', TRUE);

		// Cargar datos de cabecera
		$this->data['mainB'] = $this->Blog_m->get_main_blogs();
	} 

	// Realiza el login
	public function login_user()
	{
		$email = $this->input->post('email');
		$pass = $this->input->post('pass');

		if( $email && $pass) {
			$user = $this->Usuario_m->validate_user($email,$pass);
			if($user) {
				$this->session->set_userdata('userdata', $user);
				$this->data['userdata'] = $user;
				
				$this->load->view('privada/perfil', $this->data);
			}
			else {
				$this->data['error'] = "El email y/o la contraseña utilizados son incorrectos.";
				$this->load->view('privada/login', $this->data);
			}
		} else {
				$this->data['error'] = "Faltan datos por introducir.";
				$this->load->view('privada/login', $this->data);
		}
	}

	// Realiza el logout
	public function logout_user()
	{
		// Cargar la sesión
		$user = $this->session->userdata('userdata');
		if($user) {
			$this->data['logout'] = "Has cerrado sesión. ¡Hasta pronto ".  $user->nombre ."!";
			$this->session->unset_userdata('userdata');
		}

		anchor();

		$this->data['mainB'] = $this->Blog_m->get_main_blogs();
		$this->load->view('publica/index', $this->data);
	}

	// Realiza el registro
	public function signup_user()
	{
		$alias = $this->input->post('alias');
		$email = $this->input->post('email');
		$pass1 = $this->input->post('pass1');
		$pass2 = $this->input->post('pass2');

		$nombre = $this->input->post('nombre');
		$apellidos = $this->input->post('apellidos');
		$fechaNac = $this->input->post('fechaNac');
		$sexo = $this->input->post('sexo');

		if(strcmp($pass1,$pass2)==0) {
			if($this->Usuario_m->checkEmail($email)) {
				$new = array(
					'alias' => $alias,
					'email' => $email,
					'password' => $pass1,
					'nombre' => $nombre,
					'apellidos' => $apellidos
				);

				$id = $this->Usuario_m->new_user($new);
				if($id) {
					$user = $this->Usuario_m->get_byId($id);

					$this->session->set_userdata('userdata', $user);
					$this->data['userdata'] = $user;
					
					$this->load->view('privada/perfil', $this->data);
				}
				else {
					$this->data['heading'] = "Error 500.";
					$this->data['message'] = "Se ha producido un error en el servidor. Inténtelo más tarde.";
					$this->load->view('errors/error_500', $this->data);
				}
			}
			else {
				$error = "El email aportado ya está registrado, intente recuperar su <a href=\"#'\" class=\"alert-link\">contraseña</a>.";
				$this->form_signup($error);
			}
		}
		else {
			$error = "Las contraseñas no son las mismas.";
			$this->form_signup($error);
		}

		/*
			1. comprobar que el email es único
			2. comprobar contraseñas distintas

			3. hashear contraseña con:
				# use whatever escaping function your db requires this is very important.
				$escapedName = mysql_real_escape_string($_POST['name']); 
				$escapedPW = mysql_real_escape_string($_POST['password']);

				# generate a random salt to use for this account
				$salt = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_URANDOM));

				$saltedPW =  $escapedPW . $salt;

				$hashedPW = hash('sha256', $saltedPW);

				$query = "insert into user (name, password, salt) values ('$escapedName', '$hashedPW', '$salt'); ";
			
			4. Para el login:
				$escapedName = mysql_real_escape_string($_POST['name']);
				$escapedPW = mysql_real_escape_string($_POST['password']);

				$saltQuery = "select salt from user where name = '$escapedName';";
				$result = mysql_query($saltQuery);
				# you'll want some error handling in production code :)
				# see http://php.net/manual/en/function.mysql-query.php Example #2 for the general error handling template
				$row = mysql_fetch_assoc($result);
				$salt = $row['salt'];

				$saltedPW =  $escapedPW . $salt;

				$hashedPW = hash('sha256', $saltedPW);

				$query = "select * from user where name = '$escapedName' and password = '$hashedPW'; ";

				# if nonzero query return then successful login

			Referencia: http://stackoverflow.com/questions/6781931/how-do-i-create-and-store-md5-passwords-in-mysql
		*/
	}


	// Recupera la contraseña
	public function recover()
	{
		$this->data['recuperada'] = "TODO. Recuperar contraseña.";
		$this->load->view('privada/login', $this->data);
	}

	// Carga el formulario de login
	public function form_login()
	{
		$this->data['titulo'] = 'Iniciar sesión';
		$this->load->view('privada/login', $this->data);
	}

	// Carga el formulario de sign up
	public function form_signup($error=null)
	{
		$this->load->helper('form');
		if(!is_null($error)){
			$this->data['error'] = $error;
			$this->load->view('privada/signup', $this->data);
		}
		else {
			$this->load->view('privada/signup', $this->data);
		}
	}
}
