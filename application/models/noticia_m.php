<?php

class Noticia_m extends CI_Model {
	private $table = "noticias"; // atributo con el nombre de la tabla con la que trabajamos
	
	public function __construct() {
		parent::__construct();
		
	}


	public function get_all() {
		$this->db->select('titulo, cabecera, urlContenido, fecha', 'autor', 'blog');
		$this->db->order_by('fecha desc');
		return $this->db->get($this->table)->result();
	}

	public function count_all() {
		return $this->db->count_all($this->table);
	}

	public function is_shared() {
		$shared = false;
		$this->db->select('count(*)');
		$this->db->from('compartidas, noticia');
		$this->db->where('compartida.noticia = noticia.id');
		$num = $this->db->get()->result();
		if($num > 0) {
			$shared = true;
		}
		return $shared;
	}

	public function get_blog() {
		$this->db->select('nombre');
		$this->db->from('blogs');
		$this->db->where('blogs.id = blog');
		$blog = $this->db->get()->result();
		return $blog;
	}

	public function get_autor() {
		$this->db->select('autor');
		$this->db->from('blogs', 'usuario');
		$this->db->where('usuario.id = autor');
		$autor = $this->db->get()->result();
		return $autor;
	}

	// Obtiene noticia a partir del id
	public function get_byId($id) {
		$escapeId = mysql_real_escape_string($id);

		$result = $this->db->get_where($this->table, array('id' => $escapeId ))->row();
		return $result;
	}

	//Obtiene el conjunto de comentarios que tiene una noticia
	public function get_comments($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('*');
		$this->db->from('comentario');
		$this->db->where(array('noticia' => $escapeId));
		$this->db->order_by('fecha');
		$comentarios = $this->db->get()->result();
		return $comentarios;
	}

	//Obtiene el total de comentarios de una noticia individual
	public function get_total_comments($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('c.noticia');
		$this->db->select('count(c.id) as total');
		$this->db->from('comentario c');
		$this->db->where(array('c.noticia'=>$escapeId));
		$total = $this->db->get()->row();
		return $total;
	}

	//Obtiene el total de respuestas de cada comentario en una noticia
	public function get_total_replies($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('c.*');
		$this->db->select('count(c2.id) as total');
		$this->db->from('comentario c');
		$this->db->join('comentario c2', 'c2.replyTo = c.id', 'left');
		$this->db->where(array('c.noticia'=>$escapeId));
		$this->db->group_by('c.id');
		$this->db->order_by('fecha');
		$total = $this->db->get()->result();
		return $total;
	}


	//Obtiene el usuario de cada comentario que tiene una noticia
	public function get_user_comments($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('comentario.id as comentario');
		$this->db->select('usuario.*');
		$this->db->from('comentario');
		$this->db->join('usuario', 'usuario.id = comentario.autor');
		$this->db->where(array('noticia' => $escapeId));
		$this->db->order_by('fecha');
		$comentarios = $this->db->get()->result();
		return $comentarios;
	}


	//Inserta un comentario nuevo en la noticia
	public function new_comment($id, $autor, $filename) {
		$reply = array(
		   'id' => null,
		   'fecha' => null,
		   'urlContenido' => $filename,
		   'autor' => $autor->id,
		   'noticia' => $id,
		   'replyTo' => null
		);
		$this->db->insert('comentario', $reply);
	}

	//Inserta una respuesta a un comentario
	public function new_reply($comentario, $autor, $filename) {
		$this->db->trans_start();
		$reply = array(
		   'id' => null,
		   'fecha' => null,
		   'urlContenido' => $filename,
		   'autor' => $autor->id,
		   'noticia' => $comentario->noticia,
		   'replyTo' => $comentario->id
		);
		$this->db->insert('comentario', $reply);
		$this->db->trans_complete();
	}


	//Inserta una noticia nueva
	public function new_article($id, $autor, $titulo, $cabecera, $urlImagen, $filename, $draft) {
		$this->db->trans_start();
		$article = array(
		   'id' => null,
		   'titulo' => $titulo,
		   'cabecera' => $cabecera,
		   'urlContenido' => $filename,
		   'autor' => $autor->id,
		   'blog' => $id,
		   'urlImagen' => $urlImagen,
		   'draft' => $draft
		);
		$this->db->insert('noticias', $article);
		$this->db->trans_complete();
	}




}

?>