<?php

class Blog_m extends CI_Model {
	private $table = "blogs"; // atributo con el nombre de la tabla con la que trabajamos


	function __construct() {
		parent::__construct();
		$this->load->library('table');
	}


	//Obtiene el total de noticias de un blog
	public function get_total_articles($id='1',$pag='0') {
		$escapeId = mysql_real_escape_string($id);
		
		$min = $pag*10;
		$infinito = 18446744073709;
		$this->db->select('count(*) as total');
		$this->db->from('(select * from noticias n where n.blog = '.$escapeId.' and n.draft = 0 limit '.$infinito.' offset '.$min.') as s');
		return $this->db->get()->row();
	}

	// Obtiene todos los blogs
	public function get_all() {
		$this->db->select('nombre, urlPic, descripcion, blog_padre');
		$this->db->order_by('nombre desc');
		return $this->db->get($this->table)->result();
	}

	// Obtiene los blogs principales
	public function get_main_blogs () {
		$this->db->select('*');
		$this->db->where(array('main' => 1));
		return $this->db->get($this->table)->result();
	}

	// Comprueba si es un blog principal
	public function is_main($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->from($this->table);
		$this->db->where('id',$escapeId);
		$this->db->where('blog_padre IS NULL');
		$blog = $this->db->get()->result();

		if ( is_array($blog) && count($blog) == 1 ) {
			return true;
		}
	
		return false;
	}

	public function count_all() {
		return $this->db->count_all($this->table);
	}

	// Obtiene el padre del blog $id
	public function featured_by($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('blog_padre');
		$this->db->where('id', $escapeId);
		$padre = $this->db->get($this->table)->result()->row();
		$result = $this->db->get_where ($this->table, array('id' => $padre))->row();
		return $result;
	}

	// Obtiene los blogs hijos de $id
	public function get_list_featured($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('*');
		$this->db->where('blog_padre',$escapeId);
		return $this->db->get($this->table)->result();
	}

	// Obtiene blog a partir del id
	public function get_byId($id) {
		$escapeId = mysql_real_escape_string($id);

		$result = $this->db->get_where($this->table, array('id' => $escapeId ))->row();
		return $result;
	}


	// Crea un blog a partir de parámetros
	public function create_blog($id, $propietario, $array) {
		$escapeId = mysql_real_escape_string($id);
		$this->db->insert('blogs', $array);
		$arrayBlogUser = array(
			'autor' => $propietario,
			'blog' => $array['id'],
			'personal' => 1,
		);

		$this->db->insert('blogs_usuario', $arrayBlogUser);
	}

	// Modifica algunos parámetros
	public function update_blog($id,$array) {
		$escapeId = mysql_real_escape_string($id);
		$this->db->where('id', $escapeId);
		return $this->db->update($this->table, $array);
	}

	// Obtiene las 10 últimas noticias de un blog por su id
	public function get_ultimasnoticias($id, $pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('*');
		$this->db->from('(select * from noticias where blog='.$escapeId.' and draft = 0 order by fecha desc) as s');
		$this->db->limit(10);
		$this->db->offset($min);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get()->result();
	}

	// Obtiene las noticias más populares de un blog por su id
	public function get_popular($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('noticias.*');
		$this->db->from('comentario');
		$this->db->where('comentario.noticia = noticias.id');
		$this->db->where(array('noticias.blog' => $escapeId));
		$this->db->where('noticias.draft = 0');
		$this->db->group_by('noticias.id');
		$this->db->order_by('count(comentario.id)', 'desc');
		$this->db->order_by('noticias.fecha');
		$this->db->limit('3');
		return $this->db->get('noticias')->result();
	}

	//Obtiene las noticias más populares de todo kinja
	public function get_trending() {
		$this->db->select('noticias.*');
		$this->db->select('count(comentario.id)');
		$this->db->from('comentario');
		$this->db->where('comentario.noticia = noticias.id');
		$this->db->where('noticias.draft = 0');
		$this->db->group_by('noticias.id');
		$this->db->order_by('count(comentario.id)', 'desc');
		$this->db->order_by('noticias.fecha');
		$this->db->limit('3');
		return $this->db->get('noticias')->result();
	}

	//Obtiene el blog de cada noticia del trending on kinja
	public function get_blog_trending() {
		$this->db->select($this->table.'.nombre');
		$this->db->select('noticias.*');
		$this->db->from('comentario');
		$this->db->join($this->table, $this->table.'.id = noticias.blog');
		$this->db->where('comentario.noticia = noticias.id');
		$this->db->where('noticias.draft = 0');
		$this->db->group_by('noticias.id');
		$this->db->order_by('count(comentario.id)', 'desc');
		$this->db->order_by('noticias.fecha');		
		$this->db->limit('3');
		return $this->db->get('noticias')->result();
	}

	//Obtiene el usuario para cada noticia del listado de últimas 
	public function get_user_ultimas($id, $pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('nombre');
		$this->db->from('(select u.nombre as nombre, n.fecha as fecha from usuario u, noticias n where u.id = n.autor and n.blog = '.$escapeId.' and n.draft = 0 order by n.fecha desc) as s');
		$this->db->limit(10);
		$this->db->offset($min);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get()->result();
	}

	//Obtiene el usuario para cada noticia del listado de populares
	public function get_user_popular($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('usuario.nombre');
		$this->db->from('comentario');
		$this->db->join('usuario','usuario.id = noticias.autor');
		$this->db->where('comentario.noticia = noticias.id');
		$this->db->where(array('noticias.blog' => $escapeId));
		$this->db->where('noticias.draft = 0');
		$this->db->group_by('noticias.id');
		$this->db->order_by('count(comentario.id)', 'desc');
		$this->db->order_by('noticias.fecha');
		$this->db->limit('3');
		return $this->db->get('noticias')->result();
	}


	//Obtiene el número de comentarios que tienen las últimas noticias de un blog
	public function get_ultimascomments($id, $pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('total');
		$this->db->from('(select count(c.id) as total, n.fecha as fc from comentario c right join noticias n on n.id = c.noticia where n.blog='.$escapeId." and n.draft = 0 group by n.id order by n.fecha desc) as s");
		$this->db->limit('10');
		$this->db->offset($min);
		$this->db->order_by('fc', 'desc');
		$total = $this->db->get()->result();
		return $total;
	}

	//Obtiene las 4 siguientes noticias de un blog
	public function get_more($id,$pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('*');
		$this->db->from('(select * from noticias where blog='.$escapeId.' and noticias.draft = 0 order by fecha desc) as s');
		$this->db->limit(4);
		$this->db->offset($min);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get()->result();
	}

	// Obtiene el blog personal del usuario indicado
	public function get_byAutor($autor) {
		$escapeId = mysql_real_escape_string($autor);
		$query = $this->db->query('select * from blogs where blogs.id = ( select blog from blogs_usuario b where b.personal = 1 and b.autor = '.$escapeId.');');
		return $query->row();
	}

	// Obtiene los usuarios que pueden publicar en el blog
	public function get_privilegiados($id){
		$escapeId = mysql_real_escape_string($id);
		$query = $this->db->query('select u.id, u.nombre from usuario u, blogs_usuario b where u.id = b.autor and b.blog = 1 ');
		return $query->result();
	}

}

?>