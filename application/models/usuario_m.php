<?php

class Usuario_m extends CI_Model {
	private $table = "usuario"; // atributo con el nombre de la tabla con la que trabajamos
	private $id;
	private $nombre;
	private $email;

	public function __construct() {
		parent::__construct();
		
	}

	// Crea un usuario nuevo y su blog personal
	public function new_user($array) {
	   $this->db->trans_start();

	   // Crear el usuario
	   $this->db->set($array);
	   $this->db->insert($this->table, $array);
	   $inserted_id = $this->db->insert_id();

	   // Crear el blog personal
	   $blog = array(
	   		'nombre' => $array['nombre'],
	   		'descripcion' => 'Blog personal del usuario '.$array['nombre']
	   );
	   $this->db->set($blog);
	   $this->db->insert("blogs",$blog);
	   $inserted_blog = $this->db->insert_id();

	   // Asociarlos
	   $relacion = array(
	   		'blog' => $inserted_blog,
	   		'autor' => $inserted_id,
	   		'personal' => 1
	   );
	   $this->db->insert("blogs_usuario",$relacion);

	   $this->db->trans_complete();
	   return  $inserted_id;
	}

	// Obtiene usuario a partir del id
	public function get_byId($id) {
		$escapeId = mysql_real_escape_string($id);

		$result = $this->db->get_where($this->table, array('id' => $escapeId ))->row();
		return $result;
	}

	// Modifica algunos parámetros del usuario
	public function update_user($id, $array) {
		$escapeId = mysql_real_escape_string($id);
		$this->db->where('id', $escapeId);
		return $this->db->update($this->table, $array);
	}

	//Obtiene el total de noticias de un user
	public function get_total_articles($id='1',$pag='0') {
		$escapeId = mysql_real_escape_string($id);
		
		$min = $pag*10;
		$infinito = 18446744073709;
		$this->db->select('count(*) as total');
		$this->db->from('(select * from noticias n where n.autor = '.$escapeId.' and n.draft = 0 limit '.$infinito.' offset '.$min.') as s');
		return $this->db->get()->row();
	}

	public function get_all() {
		$this->db->select('nombre, email, , admin, urlPic');
		$this->db->order_by('nombre desc');
		return $this->db->get($this->table)->result();
	}

	public function count_all() {
		return $this->db->count_all($this->table);
	}

	public function get_comments() {
		$this->db->select('comentario.id');
		$this->db->from('comentario, usuario');
		$this->db->where('comentario.autor = usuario.id');
		return $this->db->get()->result();
	}

	// Valida al usuario para el logueo
	public function validate_user($email, $pass) {
		$escapeEmail = mysql_real_escape_string($email);
		$escapePass = mysql_real_escape_string($pass);

		$this->db->from($this->table);
		$this->db->where('email',$escapeEmail);
		$this->db->where( 'password', $escapePass);
		$login = $this->db->get()->result();

		if ( is_array($login) && count($login) == 1 ) {
			$this->details = $login[0];
			return $login[0];
		}
	
		return false;
	}

	// Comprueba que el email es único
	public function checkEmail($email) {
		$escapeEmail = mysql_real_escape_string($email);

		$this->db->select('*');
		$this->db->where(array('email' => $escapeEmail ));
		$result = $this->db->get($this->table)->result();

		if( is_array($result) && count($result) == 0) {
			return true;
		}
		return false;
	}

	public function get_nombres($nombre) {
		$escapeNombre = mysql_real_escape_string($nombre);

		$this->db->select('*');
		$this->db->like('nombre',$escapeNombre);
		$result = $this->db->get($this->table);

		if($result->num_rows > 0) {
			foreach ($result->result_array() as $row) {
				/* Para devolver más */
				$new_row['label'] = htmlentities(stripslashes($row['nombre']));
				$new_row['value'] = htmlentities(stripslashes($row['id']));
				$row_set[] = $new_row;
				/* Para devolver sólo los nombres
				$row_set[] = htmlentities(stripslashes($row['nombre']));
				*/
			}
			return json_encode($row_set);
		}
	}

	// Obtiene las 10 últimas noticias de un autor por su id
	public function get_ultimasnoticias($id, $pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('*');
		$this->db->from('(select * from noticias where autor='.$escapeId.' and noticias.draft = 0 order by fecha desc) as s');
		$this->db->limit(10);
		$this->db->offset($min);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get()->result();
	}

	// Obtiene las noticias más populares de un usuario por su id
	public function get_popular($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('noticias.*');
		$this->db->from('comentario');
		$this->db->where('comentario.noticia = noticias.id');
		$this->db->where(array('noticias.autor' => $id));
		$this->db->where('noticias.draft = 0');
		$this->db->group_by('noticias.id');
		$this->db->order_by('count(noticias.id)', 'desc');
		$this->db->limit('3');
		return $this->db->get('noticias')->result();
	}

	// Obtiene los blogs que puede modificar un autor por su id
	public function get_blogs($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('blogs.*');
		$this->db->from('blogs_usuario');
		$this->db->where('blogs_usuario.autor',$escapeId);
		$this->db->where('blogs_usuario.blog', 'blogs.id');

		return $this->db->get('blogs')->result();
	}

	//Obtiene el número de comentarios que tienen las últimas noticias de un user
	public function get_ultimascomments($id,$pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('total');
		$this->db->from('(select count(c.id) as total, n.fecha as fc from comentario c right join noticias n on n.id = c.noticia where n.autor='.$escapeId." and n.draft = 0 group by n.id order by n.fecha desc) as s");
		$this->db->limit(10);
		$this->db->offset($min);
		$this->db->order_by('fc', 'desc');
		$total = $this->db->get()->result();
		return $total;
	}


	//Obtiene las 3 últimas noticias de un usuario
	public function get_writtenBy($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('*');
		$this->db->where(array('autor' => $escapeId ));
		$this->db->where('draft = 0');
		$this->db->limit('3');
		$this->db->order_by('fecha', 'desc');
		return $this->db->get('noticias')->result();
	}

	//Obtiene las 4 siguientes noticias de un blog
	public function get_more($id, $pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('*');
		$this->db->from('(select * from noticias where autor='.$escapeId.' and noticias.draft = 0 order by fecha desc) as s');
		$this->db->limit(4);
		$this->db->offset($min);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get()->result();
	}


	// Obtiene los 10 últimos comentarios de un autor por su id
	public function get_last_comments($id, $pag) {
		$escapeId = mysql_real_escape_string($id);

		$min = $pag*10;
		$this->db->select('*');
		$this->db->from('(select * from comentario where autor='.$escapeId.' order by fecha desc) as s');
		$this->db->limit(10);
		$this->db->offset($min);
		$this->db->order_by('fecha', 'desc');
		return $this->db->get()->result();
	}


	// Obtiene el total de comentarios de un user
	public function get_total_comments($id='1', $pag='0') {
		$escapeId = mysql_real_escape_string($id);
		
		$min = $pag*10;
		$infinito = 18446744073709;
		$this->db->select('count(*) as total');
		$this->db->from('(select * from comentario c where c.autor = '.$escapeId.' limit '.$infinito.' offset '.$min.') as s');
		return $this->db->get()->row();
	}



	//Obtiene todos los borradores de un user
	public function get_drafts($id) {
		$escapeId = mysql_real_escape_string($id);

		$this->db->select('noticias.*');
		$this->db->from('noticias, usuario');
		$this->db->where(array('noticias.autor' => $escapeId));
		$this->db->where('noticias.draft = 1');
		$this->db->group_by('noticias.id');
		return $this->db->get()->result();
	}
}

?>