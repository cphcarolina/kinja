<?php

class Comentario_m extends CI_Model {
	private $table = "comentario"; // atributo con el nombre de la tabla con la que trabajamos

	public function __construct() {
		parent::__construct();
		
	}

	// Obtiene noticia a partir del id
	public function get_byId($id) {
		$escapeId = mysql_real_escape_string($id);

		$result = $this->db->get_where($this->table, array('id' => $escapeId ))->row();
		return $result;
	}


}
?>