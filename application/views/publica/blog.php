<!-- Helpers -->
<?php $this->load->helper('simpleimage'); ?>

<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>

<main class="container">
  <div class="row">
	<div class="col-md-3" id="lateral">
		<!-- Listado Trending -->
		<?php $this->load->view('inc/listados/publica/trending'); ?>
		<!-- Listado Popular -->
		<?php $this->load->view('inc/listados/publica/popular'); ?>
	</div>
	<!-- /contenido -->
	<div class="col-md-8" id="contenido">
		<!-- Logo -->
		<div id="logo"><?php if($blog->urlPic!=null) 
			echo anchor('publica/blog/'.$blog->id, "<img src=".base_url($blog->urlPic).">"); ?>
		</div>
		<!-- ultimas noticias -->
		<?php $this->load->view('inc/listados/publica/ultimas'); ?>

		<!-- listado more -->
		<?php $this->load->view('inc/listados/publica/more'); ?>

	</div>
  </div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>