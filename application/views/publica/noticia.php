<!-- Helpers e includes -->
<?php $this->load->helper('simpleimage'); ?>


<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-4" id="lateral">
			<!-- Listado Trending -->
			<?php $this->load->view('inc/listados/publica/trending'); ?>
			<!-- Listado Written by -->
			<?php $this->load->view('inc/listados/publica/writtenBy'); ?>
		</div>
		<!-- /contenido -->
		<div class="col-md-8" id="contenido">
			<!-- Logo -->
			<div id="logo"><?php if($blog->urlPic!=null) 
				echo anchor('publica/blog/'.$blog->id, "<img src=".base_url($blog->urlPic).">"); ?>
			</div>
			<!-- noticia -->
			<?php $this->load->view('inc/listados/publica/noticia'); ?>
			<!-- total de respuestas -->
			<div id="respuestas" align="right">
				<?php if($total_comments->total > 0) {
					echo ("<b>".$total_comments->total."</b> ");
					echo ("<img src=".base_url('assets/images/kinja/answers_icon.png').">");
				}
				?>
				<!-- cuadro de escribir respuesta -->
				<?php if(isset($userdata)) {
					if(isset($ckeditor_comment)) { 
						echo("<br />");
				?>
				<form class="form" id="form-ckeditor" action=<?php echo(base_url('index.php/publica/enviar_comentario/'.$noticia->id));?> method="POST">
					<div id="ckeditor" align="left">
						<textarea class="form-control" name="content" id="text-editor" ></textarea>
						<?php echo display_ckeditor($ckeditor_comment); ?>
					</div>
					<br />
					<div class="col-md-9" id="publicar">
						<input class="btn btn-info" type="submit" value="Publicar"> 
					</div>
				</form>
				<?php
					echo("<br />");
					}
					else if($noticia->draft==0) echo anchor('publica/escribir_comentario/'.$noticia->id, " Reply<br />");

				}
				else echo ("<br />");
				?>
			</div>

			<!-- comentarios -->
			<?php $this->load->view('inc/listados/publica/comentarios'); ?>
		</div>
	</div>
<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>
