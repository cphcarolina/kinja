<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>

<main class="container">
	 <?php if (isset($logout) && $logout): ?>
		<div class="alert alert-info alert-dismissable">
  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4><?php echo($logout); ?></h4>
		</div>
	<?php endif; ?>
	<?php if (isset($error) && $error): ?>
		<div class="alert alert-danger alert-dismissable">
  			<button type="button" class="close" data-dismiss="alert">&times;</button>
			<h4><?php echo($error); ?></h4>
		</div>
	<?php endif; ?>

	<div>
		<?php
			foreach ($mainB as $blog) {
		?>
				<div>
				<tr>
				<td><?php echo("<h1>");
				echo anchor('publica/blog/'.$blog->id, $blog->nombre);
				echo("</h1><br />"); ?></td>
				<td><?php echo("<p>".$blog->descripcion."</p><br />"); ?></td>
				</tr>
				</div>
		<?php
			}
		?>
	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>