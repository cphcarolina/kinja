<!-- Helpers -->
<?php $this->load->helper('simpleimage'); ?>

<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-3" id="lateral">
			<!-- Listado Popular -->
			<?php $this->load->view('inc/listados/publica/popular_user'); ?>
		</div>
		<div class="col-md-8" id="contenido">
			<h1><?php echo $usuario->nombre; ?></h1>
			<!-- noticias -->
			<?php $this->load->view('inc/listados/publica/ultimas'); ?>
			
			<!-- listado more -->
			<?php $this->load->view('inc/listados/publica/more'); ?>

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>