<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-2" id="lateral">
			<!-- Menú lateral -->
			<?php $this->load->view('inc/menuPrivada'); ?>
		</div>
		<div class="col-md-10" id="contenido">
			<h3>Your Drafts</h3>

			<?php $this->load->view('inc/listados/publica/drafts'); ?>
		</div>

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>