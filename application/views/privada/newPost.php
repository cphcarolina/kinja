<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-2" id="lateral">
			<!-- Menú lateral -->
			<?php $this->load->view('inc/menuPrivada'); ?>
		</div>
		<div class="col-md-6" id="contenido">
			<h3>Crear un nuevo post</h3>
			<form class="form" id="form-crear-post" method="POST" action=<?php echo(base_url('index.php/privada/enviar_noticia/'));?> enctype="multipart/form-data">
			<fieldset class="scheduler-border" form="formulario" name="newPost">
				<legend class="scheduler-border">Información del post</legend>
					<div class="form-group" id="groupTitulo">
						<label class="control-label" for="titulo">Título</label>
						<input class="form-control" type="text" id="titulo" name="titulo" size="30" maxlength="100"
							<?php 
								echo('required placeholder="Introduzca un título para la noticia (máximo: 100 caracteres)."');
							?>
						></input>
					</div>
					<div class="form-group" id="groupUrlImagen">
						<label class="control-label" for="imagen">Imagen portada (opcional)</label>
						<input class="form-control" type="file" id="imagen" name="imagen" size="30" maxlength="100"></input>
					</div>
					<div id="cabecera" align="left">
						<label class="control-label" for="titulo">Cabecera</label><br />
						<textarea class="form-control" name="cabecera" id="text-editor" rows="3" cols="82" required placeholder="Introduza un texto de cabecera (máximo: 250 caracteres)"></textarea>
					</div>

					<div id="ckeditor-cuerpo" align="left">
						<label class="control-label" for="titulo">Cuerpo de la noticia</label>
						<textarea class="form-control" name="content" id="text-editor"></textarea>
						<?php echo display_ckeditor($ckeditor_newPost); ?>
					</div>
			</fieldset>
			<div class="col-md-9" id="publicar">
				<input class="btn btn-info" type="submit" name="action" value="Publicar"/> 
			</div>
			<div class="col-md-13" id="drafts">
				<input class="btn btn-info" type="submit" name="action" value="Save to Drafts"/>
			</div>
			</form>


		</div>

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>