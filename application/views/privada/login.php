<script type="text/javascript">

    $(function () {
        $("#showpass").bind("click", function () {
            var txtPassword = $("#pass");
            if ($(this).is(":checked")) {
                txtPassword.after('<input class="form-control" onchange = "PasswordChanged(this);" id = "txt_' + txtPassword.attr("id") + '" type = "text" value = "' + txtPassword.val() + '" name="pass" size="12" maxlength="100" required placeholder="Introduzca su contraseña."/>');
                txtPassword.hide();
            } else {
                txtPassword.val(txtPassword.next().val());
                txtPassword.next().remove();
                txtPassword.show();
            }
        });
    });
    function PasswordChanged(txt) {
        $(txt).prev().val($(txt).val());
    }
</script>


<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<section id="contenido">
		<?php
		if(isset($timeout)) {
			echo("<p class='aviso'>Cerrada sesión por inactividad.</p>");
		}
		if(isset($recuperada)) {
			echo("<p class='aviso'>" . $recuperada . "</p>");
			// Se le ha enviado un email con sus datos. Compruebe su bandeja de entrada.
		}
		?>
		
		<h1>Login</h1>
		<div class="row" id="WTF">
			<form class="form  col-md-7" role="form" action=<?php echo site_url('login/login_user'); ?> method="POST" id="formulario" name="formulario">
				<fieldset class="scheduler-border" form="formulario" name="datosLogin">
					<legend class="scheduler-border">Datos</legend>
					<div class="control">
						<div class="form-group">
							<label class="control-label row-col" for="email">Email:</label>
							<input class="form-control" type="text" id="email" name="email" size="12" maxlength="100" 
							required placeholder="Introduzca su email."/>
						</div>
					</div>
					<div class="control">
						<div class="form-group" id="groupPass">
							<label class="control-label" for="pass">Contraseña:</label>
							<input class="form-control" type="password" id="pass" name="pass" size="12" maxlength="100" 
							required placeholder="Introduzca su contraseña."/>
							<label class="col-md-5"><input id="showpass" type="checkbox" /> Mostrar contraseña</label>
						</div>
					</div>
					<div class="form-group row" id="passperdido" style="margin-bottom:0px;">
						<p class='col-md-6 error'>
							<?php
							if(isset($error)) {
								echo($error);
							}
							?>
						</p>
						<p class="col-md-6" align="right" style="font-size:12px"><?php echo anchor('login/recover', '¿Contraseña olvidada?'); ?></p>
					</div>
				</fieldset>
				<div class="row" style="margin-bottom:5px" align="center">
					<input class="btn btn-info" type="submit" value="Iniciar sesión"/>
				</div>
				
				<div class="row" align="center">
					<p>¿No tiene todavía una cuenta? <?php echo anchor('privada/signup', 'Regístrese'); ?></p>
				</div>
			</form>
		</div>
	</section>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>