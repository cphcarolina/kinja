<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-2" id="lateral">
			<!-- Menú lateral -->
			<?php $this->load->view('inc/menuPrivada'); ?>
		</div>
		<div class="col-md-10" id="contenido">
			<h3>Notificaciones</h3>
			<div class="row">
				<li class="list-group-item">
					<p class="subtitulo">2015-01-12 16:46:45 - El usuario <a href="#">Superman</a> te ha respondido a un <a href="#">mensaje</a></p>
				</li>
			</div>
		</div>

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>