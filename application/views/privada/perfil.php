<?php $this->load->view('inc/cabecera'); ?>

<main class="container">
	<div class="row" id="menu">
		<div class="col-md-2" id="lateral">
			<!-- Menú lateral -->
			<?php $this->load->view('inc/menuPrivada'); ?>
		</div>

		<div class="col-md-10 container" id="contenido">
			<h3>Perfil</h3>
			<?php if (isset($actualizado) && $actualizado): ?>
				<div class="alert alert-info alert-dismissable">
		  			<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4><?php echo($actualizado); ?></h4>
				</div>
			<?php endif; ?>
			<div class="row">
				<?php if(!isset($currentTab)) $currentTab = 'infUser' ?>

				<script type="text/javascript">
					jQuery(document).ready(function ($) {
						$('#tabs').tab();
					});
				</script>
 
				<!-- Menú -->
				<ul id="tabs" class="nav nav-tabs col-md-7" data-tabs="tabs">
					<li<?php if(strcmp("infUser", $currentTab) == 0) echo ' class="active"'; ?>>
						<a href="#infUser" data-toggle="tab">Cuenta de usuario</a>
					</li>
					<li<?php if(strcmp("avatar", $currentTab) == 0) echo ' class="active"'; ?>>
						<a href="#avatar" data-toggle="tab">Avatar</a>
					</li>
					<li<?php if(strcmp("infPersonal", $currentTab) == 0) echo ' class="active"'; ?>>
						<a href="#infPersonal" data-toggle="tab">Información personal</a>
					</li>
				</ul>
		
				<div id="my-tab-content" class="tab-content">
					<!-- Información del usuario -->
					<div id="infUser"
						<?php 
							if(strcmp("infUser", $currentTab) == 0) echo 'class="tab-pane active"';
							else echo 'class="tab-pane"';
						?>>
						<form class="form" role="form" 
							action=<?php echo site_url('privada/edit_InfUser'); ?>
							method="POST" id="editInfUser" name="formulario">

							<?php $this->load->view('inc/privada/form_infUser'); ?>
							<div class="form-group col-md-7">
								<input class="btn btn-info" type="submit" value="Enviar los datos"/>
							</div>
						</form>
					</div>

					<!-- Avatar -->
					<div  id="avatar"
						<?php 
							if(strcmp("avatar", $currentTab) == 0) echo 'class="tab-pane active"';
							else echo 'class="tab-pane"';
						?>>
						<form class="form" role="form" 
							action=<?php echo site_url('privada/edit_Avatar'); ?>
							method="POST" id="editAvatar" name="formulario" enctype="multipart/form-data">
							
							<?php $this->load->view('inc/privada/form_img'); ?>
							<div class="form-group col-md-7">
								<input class="btn btn-info" type="submit" value="Enviar los datos"/>
							</div>
						</form>
					</div>

					<!-- Información personal -->
					<div id="infPersonal"
						<?php 
							if(strcmp("infPersonal", $currentTab) == 0) echo 'class="tab-pane active"';
							else echo 'class="tab-pane"';
						?>>
						<form class="form" role="form" 
							action=<?php echo site_url('privada/edit_InfPersonal'); ?>
							method="POST" id="editInfPersonal" name="formulario">
							
							<?php $this->load->view('inc/privada/form_infPersonal'); ?>
							<div class="form-group col-md-7">
								<input class="btn btn-info" type="submit" value="Enviar los datos"/>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>