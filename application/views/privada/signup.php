<?php $this->load->view('inc/cabecera'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/registro.js"></script>


<section class="container">
	<div class="row"><div class="col-md-7">
		<h1>Formulario de registro</h1>
		<p class="subtitulo">Para poder disfrutar de las ventajas de nuestros servicios, deberá rellenar el siguiente formulario. Los datos aportados se utilizarán únicamente con el fin de ofrecerles un mejor servicio y no serán empleados por terceros.</p>
		<p style="font-size:85%" align="right">Todos los campos son obligatorios, excepto si se indica lo contrario.</p>
		<?php if (isset($error) && $error): ?>
			<div class="alert alert-warning alert-dismissable">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				<h4><?php echo($error); ?></h4>
			</div>
		<?php endif; ?>
	</div></div>
</section>

<main class="container">
<!-- el required no lo aceptan todos los navegadores, porque
no todos suportan html5, por lo que debería haber una función 
js que comprueben los datos-->
	<form class="form row" role="form" action=<?php echo site_url('login/signup_user'); ?> method="POST" id="formulario" name="formulario">
		<?php $this->load->view('inc/privada/form_infUser'); ?>
		<?php $this->load->view('inc/privada/form_infPersonal'); ?>
		
		<div class="col-md-7"><p style="font-size:85%" align="right">Todos los campos son obligatorios, excepto si se indica lo contrario.</p>
		</div>
		<div class="form-group col-md-7">
			<input class="btn btn-info" type="submit" value="Enviar los datos"/>
		</div>
	</form>


<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>