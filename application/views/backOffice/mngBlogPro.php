<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-2" id="lateral">
			<?php $this->load->view('inc/menuBlogs'); ?>
		</div><!-- /lateral -->

		<div class="col-md-10" id="contenido">
			<h3>Modificar blog</h3>
			<?php if (isset($actualizado) && $actualizado): ?>
				<div class="alert alert-info alert-dismissable">
		  			<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4><?php echo($actualizado); ?></h4>
				</div>
			<?php elseif(isset($error) && $error) : ?>
				<div class="alert alert-info alert-dismissable">
		  			<button type="button" class="close" data-dismiss="alert">&times;</button>
					<h4><?php echo($error); ?></h4>
				</div>
			<?php endif; ?>
			<div class="row">
				<?php if(!isset($currentTab)) $currentTab = 'infBlog' ?>

				<script type="text/javascript">
					jQuery(document).ready(function ($) {
						$('#tabs').tab();
					});
				</script>
 
				<!-- Menú -->
				<ul id="tabs" class="nav nav-tabs col-md-7" data-tabs="tabs">
					<li<?php if(strcmp("infBlog", $currentTab) == 0) echo ' class="active"'; ?>>
						<a href="#infBlog" data-toggle="tab">Información del Blog</a>
					</li>
					<li<?php if(strcmp("logo", $currentTab) == 0) echo ' class="active"'; ?>>
						<a href="#logo" data-toggle="tab">Logo</a>
					</li>
					<li<?php if(strcmp("priv", $currentTab) == 0) echo ' class="active"'; ?>>
						<a href="#priv" data-toggle="tab">Privilegios</a>
					</li> 
				</ul>
				<!-- Contenido -->
				<div id="my-tab-content" class="tab-content">
					<!-- Información del blog -->
					<div id="infBlog"
						<?php 
							if(strcmp("infBlog", $currentTab) == 0) echo 'class="tab-pane active"';
							else echo 'class="tab-pane"';
						?>>
						<form class="form" role="form" 
							action=<?php echo site_url('privada/edit_InfBlog'); ?>
							method="POST" id="editInfBlog" name="formulario">

							<?php $this->load->view('inc/privada/form_infBlog'); ?>
							<div class="form-group col-md-7">
								<input class="btn btn-info" type="submit" value="Enviar los datos"/>
							</div>
						</form>
					</div>

					<!-- Logo -->
					<div  id="logo"
						<?php 
							if(strcmp("logo", $currentTab) == 0) echo 'class="tab-pane active"';
							else echo 'class="tab-pane"';
						?>>
						<form class="form" role="form" 
							action=<?php echo site_url('admin/edit_Logo'); ?>
							method="POST" id="editLogo" name="formulario">
							
							<?php $this->load->view('inc/privada/form_img'); ?>
							<div class="form-group col-md-7">
								<input class="btn btn-info" type="submit" value="Enviar los datos"/>
							</div>
						</form>
					</div>

					<!-- Privilegios del blog -->
					<div id="priv"
						<?php 
							if(strcmp("priv", $currentTab) == 0) echo 'class="tab-pane active"';
							else echo 'class="tab-pane"';
						?>>
						<form class="form" role="form" 
							action=<?php echo site_url('#'); ?>
							method="POST" id="editInfBlog" name="formulario">

							<?php $this->load->view('inc/privada/form_blogPriv'); ?>
							<div class="form-group col-md-7">
								<input class="btn btn-info" type="submit" value="Enviar los datos"/>
							</div>
						</form>
					</div>

				</div>
			</div>
		</div><!-- /contenido -->

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>