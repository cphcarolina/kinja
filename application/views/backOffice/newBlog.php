<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-2" id="lateral">
			<?php $this->load->view('inc/menuBlogs'); ?>
		</div><!-- /lateral -->

		<div class="col-md-10" id="contenido">
			<h3>Crear un nuevo blog</h3>
			<?php $this->load->view('inc/privada/form_infBlog'); ?>
			<div class="form-group col-md-7">
				<input class="btn btn-info" type="submit" value="Crear Blog"/>
			</div>
		</div><!-- /contenido -->

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>