<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>


<main id="wrap" class="container">
	<div class="row">
		<div class="col-md-2" id="lateral">
			<?php $this->load->view('inc/menuBlogs'); ?>
		</div><!-- /lateral -->

		<div class="col-md-10" id="contenido">
			<div id="buscador">
				<form class="form-inline pull-right">
					<label class="control-label" for="nombre">Blogs:</label>
					<input class="form-control" type="text" id="blog" onkeyup="autocomplet()">
					<input class="btn btn-info" type="submit" value="Buscar"/>
				</form>
			</div>

			<h3>Blogs relacionados</h3>

			<div class="col-md-12">
				<ol class="list-group">
					<?php 
						echo anchor('admin/form_mngBlogPro/'.$padre->id, '<li class="list-group-item">'.$padre->nombre.'</li>');
						foreach ($otherB as $blog) {
							echo anchor('admin/form_mngBlogPro/'.$blog->id, '<li class="list-group-item">'.$blog->nombre.'</li>');
						}
					?>
				</ol>
			</div>
		</div><!-- /contenido -->

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>

<!--



<div class="list-group">
  <a href="#" class="list-group-item active">
    Cras justo odio
  </a>
  <a href="#" class="list-group-item">Dapibus ac facilisis in</a>
  <a href="#" class="list-group-item">Morbi leo risus</a>
  <a href="#" class="list-group-item">Porta ac consectetur ac</a>
  <a href="#" class="list-group-item">Vestibulum at eros</a>
</div>



	-->