<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-2" id="lateral">
			<?php $this->load->view('inc/menuBlogs'); ?>
		</div><!-- /lateral -->

		<div class="col-md-10" id="contenido">
			<div id="buscador">
				<form class="form-inline pull-right">
					<label class="control-label" for="nombre">Blogs:</label>
					<input class="form-control" type="text" id="blog" onkeyup="autocomplet()">
					<input class="btn btn-info" type="submit" value="Buscar"/>
				</form>
			</div>
			<div><img src="<?php echo base_url(); ?>/assets/images/tutorial.png"/></div>
		</div><!-- /contenido -->

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>