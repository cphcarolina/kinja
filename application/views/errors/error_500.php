<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>

<main class="container">
	<div id="container">
		<h1><?php echo $heading; ?></h1>
		<?php echo $message; ?>
	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>