<section id="noticia">
	<?php
		echo("<h1>" .($noticia->titulo). "</h1>");
		$image = new SimpleImage();
		if($autor->urlPic!=null) {
			$image->load($autor->urlPic);
			if($image->getWidth() > $image->getHeight()) {
				$image->resizeToWidth(50); 			
			}
			else $image->resizeToHeight(50); 
			$image->save($autor->urlPic."_thumbnail.png");
			echo ("<img src=".base_url($autor->urlPic."_thumbnail.png")."> ");
		}
		echo ("<font size=3>");
		echo anchor('publica/usuario/'.$autor->id, $autor->nombre."</font><br />");
		echo ("<font size=1.6>");
		echo(" at ".$noticia->fecha."</font><br />");
		if($noticia->urlImagen!=null) {
			$image->load($noticia->urlImagen);
			if($image->getWidth() > 600) {
				$image->resizeToWidth(600); 			
			}
			if($image->getHeight() > 300) {
				$image->resizeToHeight(300);
			} 
			$image->save($noticia->urlImagen."_full.png");
			print("<img src=".base_url($noticia->urlImagen."_full.png")."><br /><br />");
		}
		else echo("<br /><br />");
	?>

	<?php
		$content = file($noticia->urlContenido);
		echo("<p>");
		foreach ($content as $line) {
			echo($line);	
		}
		echo("</p>");
	?>
</section>