<section id="noticias">
	<?php foreach ($ultimas as $index=>$noticia) { ?>
		<div class="col-md-12 item" id="noticia">
			<div id="titulo">
				<h1 class="titulo">
				<?php echo anchor('publica/noticia/'.$noticia->id, $noticia->titulo); ?>
				</h1>
			</div>
			<div id="posted-by">
				<!-- <div><img src=<?php echo base_url($autor->urlPic."_thumbnail.png"); ?></div> -->
				<div>
					<p class="subtitulo"><?php 
					if (!isset ($usuario)) {
						echo anchor('publica/usuario/'.$noticia->autor, $ultimas_usuario[$index]->nombre);
					}
					else {
						echo anchor('publica/usuario/'.$noticia->autor, $usuario->nombre);
					}
					if($ultimas_comments[$index]->total>0) 
							echo (" and ".$ultimas_comments[$index]->total." others");
					?></p>
					<p class="fecha"><?php echo $noticia->fecha; ?></p>
				</div>
			</div>

			<div id="cabecera">
				<?php if($noticia->urlImagen!=null) {
					$image = new SimpleImage();
					$image->load($noticia->urlImagen);
					if($image->getWidth() > $image->getHeight()) {
						$image->resizeToWidth(130); 			
					}
					else $image->resizeToHeight(65); 
					$image->save($noticia->urlImagen."_mini.png");
				
					print("<img id=\"imagen\" class=\"miniatura\" src=".base_url($noticia->urlImagen."_mini.png").">"); 
				}
				?>
				<p><?php echo($noticia->cabecera); ?>
			</div>
			<br/>
		</div>
	<?php } ?>
</section>