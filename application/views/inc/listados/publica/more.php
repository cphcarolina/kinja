<?php if($total_articles > 10): ?>
<?php $sig_pag = $pag+1; ?>

<section id="more">
	<?php if(isset($blog)): ?><div class="col-md-8"><h3>More from <?php echo($blog->nombre."<br /><br /><br />")?></h3></div>
<?php else: ?><div class="col-md-8"><h3>More from <?php echo($usuario->nombre."<br /><br /><br />")?></h3></div>
<?php endif;?>
	<div class="row row-centered" style="clear: left; display: inline-block;">
		<?php foreach ($more as $noticia) { ?>
			<div class="col-md-3 moreItem">
				<?php if($noticia->urlImagen!=null) {
					$image = new SimpleImage();
					$image->load($noticia->urlImagen);
					if($image->getWidth() > $image->getHeight()) {
						$image->resizeToWidth(125); 			
					}
					else $image->resizeToHeight(100); 
					$image->save($noticia->urlImagen."_mini.png");

					echo anchor('publica/noticia/'.$noticia->id, "<div><img src=".base_url($noticia->urlImagen."_mini.png")."></div>");
				}?>
				<div><p class="subtitulo"><?php echo anchor('publica/noticia/'.$noticia->id, $noticia->titulo); ?></p></div>
			</div>

			<?php
				}
			?>
	</div>
	<?php if(isset($blog)) {
 		?>	
 		<div align="center"><?php 
		echo anchor('publica/blog/'.$blog->id.'/'.$sig_pag, '<input type="button" value="More stories"><br /><br />');
		?></div>		
		<?php 
	}
		else {
		?>
		<div align="center"><?php 
		echo anchor('publica/usuario/'.$usuario->id.'/'.$sig_pag, '<input type="button" value="More stories"><br /><br />');
	?></div>
	<?php 
	}
	?>

</section>
<?php endif; ?>

