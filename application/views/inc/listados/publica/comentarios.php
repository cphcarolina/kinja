<section id="comentarios">
	<h1>All replies</h1><br />
	<?php
		foreach ($comments as $index=>$comentario) {

			if($usuario_comments[$index]->urlPic!=null) {
				$image = new SimpleImage();
				$image->load($usuario_comments[$index]->urlPic);
				if($image->getWidth() > $image->getHeight()) {
					$image->resizeToWidth(50); 			
				}
				else $image->resizeToHeight(50); 
				$image->save($usuario_comments[$index]->urlPic."_thumbnail.png");
				echo ("<img src=".base_url($usuario_comments[$index]->urlPic."_thumbnail.png")."> ");
			}
			echo anchor('publica/usuario/'.$comentario->autor, "<font size=3>".$usuario_comments[$index]->nombre);
			if($comentario->replyTo!=null) {
				echo(" > ");
				foreach($usuario_comments as $user) {
					if($user->comentario == $comentario->replyTo)
						echo($user->nombre."<br />");
				}

			}
			else {
				echo(" > ");
				echo($autor->nombre."<br />");
			}
			echo("</font>");
			echo("<font size=1.6> at ".$comentario->fecha."</font><br /><br />");
			$answer = file($comentario->urlContenido);
			echo("<p>");
			foreach ($answer as $line) {
				echo($line);	
			}
	?>
		<div id="respuestas" align="right">
			<?php if($total_replies[$index]->total > 0) {
				echo ("<b>".$total_replies[$index]->total."</b> ");
				echo ("<img src=".base_url('assets/images/kinja/answers_icon.png').">");
			}
			?>
			<!-- cuadro de escribir respuesta -->
			<?php if(isset($userdata)) {
				if(isset($ckeditor_reply) && $comentario->id == $replyTo->id) { 
			?>
			<form class="form" id="form-ckeditor" action=<?php echo(base_url('index.php/publica/enviar_respuesta/'.$comentario->id));?> method="POST">
				<div id="ckeditor" align="left">
					<textarea class="form-control" name="content" id="text-editor" ></textarea>
					<?php echo display_ckeditor($ckeditor_reply); ?>
				</div>
				<br />
				<div class="col-md-6" id="publicar">
					<input class="btn btn-info" type="submit" value="Publicar"> 
				</div>
				<br />
			</form>
			<?php
				echo ("<br />");
				}
				else echo anchor('publica/escribir_respuesta/'.$comentario->id, " Reply<br />");
			}?>
		</div>
	<?php
	}
	?>
</section>