<section id="noticias">
	<?php
	foreach ($drafts as $noticia) {
		?>
			<div class="col-md-12 item" id="noticia">
			<div id="titulo">
				<h1 class="titulo">
				<?php echo anchor('publica/noticia/'.$noticia->id, $noticia->titulo); ?>
				</h1>
			</div>
			<div id="posted-by">
				<!-- <div><img src=<?php echo base_url($autor->urlPic."_thumbnail.png"); ?></div> -->
				<div>
					<p class="subtitulo"><?php 
					echo anchor('publica/usuario/'.$noticia->autor, $userdata->nombre);?>
					<p class="fecha"><?php echo $noticia->fecha; ?></p>
				</div>
			</div>

			<div id="cabecera">
				<p><?php echo($noticia->cabecera); ?>
			</div>
			<br/>
		</div>
	<?php
	}
	?>
</section>