<fieldset class="scheduler-border col-md-7" form="formulario" name="infoUser">
	<legend class="scheduler-border">Información de usuario</legend>
	<div class="form-group">
		<label class="control-label" for="alias">Alias:</label>
		<input class="form-control" type="text" id="alias" name="alias" size="12" maxlength="100" 
			<?php 
				if(isset($userdata) && $userdata) echo ('value="'.$userdata->alias.'"');
				else echo('required placeholder="Introduzca el nombre de usuario que desea."');
			?>
		></input>
	</div>
	<?php if(!isset($userdata)) : ?>
		<div class="form-group" id="groupEmail">
			<label class="control-label" for="email">Email:</label>
			<input class="form-control" type="text" id="email" name="email" size="30" maxlength="100"
				<?php 
					if(isset($userdata) && $userdata) echo ('value="'.$userdata->email.'"');
					else echo('required placeholder="Introduzca una dirección de correo válida."');
				?>
			></input> 
		</div>

		<!-- Crear contraseña -->
		<div class="form-group" id="groupPass1">
			<label class="control-label" for="pass1">Contraseña:</label>
			<input class="form-control" type="password" id="pass1" name="pass1" size="12" maxlength="100"
			required placeholder="Introduzca una contraseña segura (lim. 12 caracteres)."/>
		</div>
		<div class="form-group" id="groupPass2">
			<label class="control-label" for="pass2">Repetir contraseña:</label>
			<input class="form-control" type="password" id="pass2" name="pass2" size="12" maxlength="100"
			required placeholder="Repita la contraseña anterior."/>
		</div>	
		<div class="form-group" id="errorPass"></div>
		<div class="form-group" id="errorEmail"></div>
	<?php else : ?>
		<!-- Cambio de contraseña -->
		<fieldset class="scheduler-border" form="formulario" name="changePass">
			<legend class="scheduler-border">Cambiar la contraseña</legend>
			<div class="form-group" id="groupPass1">
				<label class="control-label" for="pass1">Nueva contraseña:</label>
				<input class="form-control" type="password" id="pass1" name="pass1" size="12" maxlength="100"
				placeholder="Introduzca una contraseña segura (lim. 12 caracteres)."/>
			</div>
			<div class="form-group" id="groupPass2">
				<label class="control-label" for="pass2">Repetir contraseña:</label>
				<input class="form-control" type="password" id="pass2" name="pass2" size="12" maxlength="100"
				placeholder="Repita la contraseña anterior."/>
			</div>
			<div class="form-group" id="groupPass3">
				<label class="control-label" for="pass3">Confirme con la contraseña anterior:</label>
				<input class="form-control" type="password" id="pass3" name="pass3" size="12" maxlength="100"
				placeholder="Repita la contraseña anterior."/>
			</div>
			<div class="form-group" id="errorPass"></div>
			<div class="form-group" id="errorEmail"></div>
		</fieldset>
	<?php endIf; ?>
</fieldset>
