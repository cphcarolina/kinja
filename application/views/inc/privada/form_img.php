<fieldset class="scheduler-border col-md-7" form="formulario" name="avatar">
	<legend class="scheduler-border">Avatar</legend>
	<div class="form-group" id="avatar">
		<input class="form-control" type="file" id="avatar" name="avatar" size="30" maxlength="100">
		<br/>
			<?php 
				if(!isset($blogdata) && isset($userdata) && $userdata->urlPic) 
					echo ('<img class="imagen" src='.base_url($userdata->urlPic).'>');
				if(isset($blogdata) && $blogdata->urlPic) {
					echo ('<img class="imagen" src='.base_url($blogdata->urlPic).'>');
				}
			?>
		</input>
	</div>
</fieldset>