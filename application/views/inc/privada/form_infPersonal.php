<fieldset class="scheduler-border col-md-7" form="formulario" name="infoPers">
	<legend class="scheduler-border">Información personal</legend>
	<div class="control row">
		<div class="control col-md-4" id="groupNombre">
			<label class="control-label" for="nombre">Nombre:</label>
			<input class="form-control" type="text" id="nombre" name="nombre" size="30" maxlength="100"
				<?php 
					if(isset($userdata) && $userdata) echo ('value="'.$userdata->nombre.'"');
					else echo('required placeholder="Introduzca su nombre."');
				?>
			></input>
		</div>
		<div class="form-group col-md-8" id="groupApellidos">
			<label class="control-label" for="apellidos">Apellidos:</label>
			<input class="form-control" type="text" id="apellidos" name="apellidos" size="30" maxlength="100"
				<?php 
					if(isset($userdata) && $userdata) echo ('value="'.$userdata->apellidos.'"');
					else echo('required placeholder="Introduzca sus apellidos."');
				?>
			></input>
		</div>
	</div>
	<div class="form-group" id="groupFecha">
		<label class="control-label" for="fechaNac">Fecha de nacimiento:
			<?php if(!isset($userdata)) echo ('(opcional)'); ?>
		</label>
		<input class="form-control" type="text" id="fechaNac" name="fechaNac" size="30" maxlength="100"
			<?php 
				if(isset($userdata) && $userdata) echo ('value="'.$userdata->fechaNac.'"');
				else echo('placeholder="Introduzca su fecha de nacimiento (DD/MM/YYYY o YYYY/MM/DD)."');
			?>
		></input>
	</div>
	<div class="form-group" id="groupSexo">
		<div>
			<label class="control-label" for="sexo">Sexo:
				<?php if(!isset($userdata)) echo ('(opcional)'); ?>
			</label>
		</div>
		<div>
			<input type="radio" name="sexo" id="sexoH" value="H"
				<?php if(isset($userdata) && $userdata && strcmp($userdata->sexo, 'hombre')==0) echo(' checked'); ?>
			>Hombre
			<input type="radio" name="sexo" id="sexoM" value="M"
				<?php if(isset($userdata) && $userdata && strcmp($userdata->sexo, 'mujer')==0) echo(' checked'); ?>
			> Mujer
		</div>
	</div>
	<div class="form-group" id="errorFecha"></div>
</fieldset>