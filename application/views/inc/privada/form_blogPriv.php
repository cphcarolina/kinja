<!--<script type="text/javascript" src="<?php //echo base_url(); ?>/assets/js/autocompletar.js"></script>-->
<script type="text/javascript">
$(document).ready(function() {
	$(function(){
	  $("#usuario").autocomplete({
	    source: "admin/get_usuarios" // path to the get_birds method
	  });
	});
});
</script>

<fieldset class="scheduler-border col-md-7" form="formulario" name="infoPers">
	<legend class="scheduler-border">Privilegios</legend>
	<div class="content">
		<form>
			<label class="control-label" for="email">Añada nuevos usuarios al blog colaborativo:</label>
			<input class="form-control" type="text" id="usuario" onkeyup="autocomplet()">
			<br/>
			<table id="listaUsuarios" class="table table-striped table-bordered">
				<tr><th>Nombre</th><th>Noticias publicadas</th><th>Opciones</th></tr>
				<tr>
				<?php foreach ($usuarios as $user) : ?>
				<tr>
					<td id="nombre" ><?php echo anchor('publica/usuario/'.$user->id, $user->nombre); ?></td>
					<td id="noticias"></td>
					<td style="text-align: right;"><?php echo anchor('admin/delete/'.$user->id, 'Eliminar'); ?></td>
				</tr>
				<?php endforeach; ?>
				</tr>
			</table>
		</form>
	</div><!-- content -->

</fieldset>