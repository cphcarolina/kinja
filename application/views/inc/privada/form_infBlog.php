<fieldset class="scheduler-border col-md-7" form="formulario" name="infoBlog">
	<legend class="scheduler-border">Información del Blog</legend>
	<div class="control" id="groupNombre">
		<label class="control-label" for="nombre">Nombre:</label>
		<input class="form-control" type="text" id="nombre" name="nombre" size="30" maxlength="100"
			<?php 
				if(isset($userBlog) && $userBlog) echo ('value="'.$userBlog->nombre.'"');
				else echo('required placeholder="Introduzca su nombre."');
			?>
		></input>
		<?php if(!isset($userBlog)) : ?>
			<div class="form-group" id="groupPadre">
				<label class="form-label">Blog padre:</label>
				<select class="form-control" name="cars">
					<option value=null>- Sin padre -</option>
					<!-- Listado de los blogs principales -->
	                <?php if(isset($mainB)):
	                  foreach ($mainB as $blog): ?>
	                  	<option value=<?php echo $blog->nombre; ?>><?php echo $blog->nombre; ?></option>
	                <?php endforeach;
	                else: ?>
	                  <?php echo '-Sin blogs-'; ?></li>
	                <?php endif; ?> <!-- /listado -->
				</select>
			</div>
			<div class="form-group" id="groupMain">
				<input type="checkbox" name="main" value="1" />
				<label class="form-label"> Blog principal</label>
			</div>
		<?php endif; ?>
	</div>
	<div class="form-group" id="groupDesc">
		<div><label class="control-label">Descripcion:</label></div>
		<div><textarea class="form-control" rows="2" name="descripcion" form="editInfBlog"><?php 
				if(isset($userBlog) && $userBlog) echo $userBlog->descripcion;
		?></textarea></div>
	</div>
</fieldset>