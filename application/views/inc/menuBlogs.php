<section class="btn-group-vertical" id="menus">
	<?php if(!isset($currentPage)) $currentPage = 'index' ?>

	<ul class="nav nav-pills nav-stacked">
		<li<?php if(strcmp("newPost", $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('admin/form_newBlog', 'New Blog'); ?></li>
	</ul>
	<br/>
	<ul class="nav nav-pills nav-stacked" id="menuListados">
		<li role="presentation" class="dropdown-header">Blogs principales</li>
		<?php foreach ($mainB as $index => $blog): ?>
			<li<?php if(strcmp($blog->nombre, $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('admin/listado/'.$blog->id, $blog->nombre); ?></li>
		<?php endforeach; ?>
	</ul>
</section>