<section class="btn-group-vertical" id="menus">
	<?php if(!isset($currentPage)) $currentPage = 'perfil' ?>

	<ul class="nav nav-pills nav-stacked" id="menuFormulario">
		<li<?php if(strcmp("perfil", $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('privada/form_perfil', 'Profile'); ?></li>
		<?php if($userdata->admin == 1) : ?>
			<li><?php echo anchor('admin/index', 'Manage Blogs'); ?></li>
		<?php endif; ?>
		<li<?php if(strcmp("newPost", $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('privada/form_post', 'Create Post'); ?></li>
	</ul>
	<br/>
	<ul class="nav nav-pills nav-stacked" id="menuListados">
		<li<?php if(strcmp("posts", $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('privada/show_posts', 'Your Posts'); ?></li>
		<li<?php if(strcmp("comments", $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('privada/show_comments', 'Your Comments'); ?></li>
		<li<?php if(strcmp("drafts", $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('privada/show_drafts', 'Drafts'); ?></li>
		<li<?php if(strcmp("notif", $currentPage) == 0) echo ' class="active"'; ?>>
			<?php echo anchor('privada/show_notifications', 'Notifications'); ?></li>
	</ul>

</section>