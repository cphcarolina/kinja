<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title><?php echo(nombre_sitio); ?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/bootstrap-theme.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/assets/css/estilos.css">
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/jquery-1.11.1.js"></script>
	<script type="text/javascript" src="<?php echo base_url(); ?>/assets/js/bootstrap.min.js"></script>
</head>

<body>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
  	<div class="navbar-header">
      <div class="navbar-brand"><?php echo anchor('publica/index', 'Home'); ?></div>
    </div>
    <div class="container-fluid">
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="container-fluid mi-navbar-left">
        <ul class="nav navbar-nav">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Discover More<span class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
              <!-- Listado de los blogs hijos -->
              <?php if(isset($otherB)): ?>
                <li role="presentation" class="dropdown-header">Related Blogs</li>
                <?php foreach ($otherB as $blog): ?>
                  <li><?php echo anchor('publica/blog/'.$blog->id, $blog->nombre); ?></li>
              <?php endforeach; ?>
              <li role="presentation" class="divider"></li>
              <?php endif; ?> <!-- /listado -->
              <li role="presentation" class="dropdown-header">Blogs you may like</li>
              <!-- Listado de los blogs principales -->
                <?php if(isset($mainB)):
                  foreach ($mainB as $blog): ?>
                    <li><?php echo anchor('publica/blog/'.$blog->id, $blog->nombre); ?></li>
                <?php endforeach;
                else: ?>
                  <li><?php echo anchor('#', '-Sin blogs-'); ?></li>
                <?php endif; ?> <!-- /listado -->
            </ul>
          </li>
        </ul>
      </div>
      <!-- Código pruebas
      <div class="container-fluid mi-navbar-left">
        <ul class="nav navbar-nav"><?php
           if(isset($userB)) { echo '<ul><p>'.'main'.'</p></ul>'; }
           else { echo '<ul><p>WTF</p></ul>'; }
        ?></ul>
      </div> -->
      
  		<div class="container-fluid mi-navbar-right">
        <ul class="nav navbar-nav navbar-right">

          <?php if(isset($userdata)): ?>
            <!-- Parte privada -->
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                <?php echo ($userdata->nombre); ?>
                <!--<img src=<?php base_url($userdata->urlPic); ?> class="img-circle special-img">-->
              <span class="caret"></a>
              <ul class="dropdown-menu" role="menu">
                <li><?php echo anchor('privada/form_perfil', 'Private view'); ?></li>
                <li><?php echo anchor('privada/form_blog', 'Manage My Blog'); ?></li>
                <li><?php echo anchor('login/logout_user', 'Log out'); ?></li>
                <li role="presentation" class="divider"></li>
                <li role="presentation" class="dropdown-header">My Blogs</li>
                <!-- Listado de los blogs del usuario -->
                <?php if(isset($userB)):
                  foreach ($userB as $blog): ?>
                    <li><?php echo anchor('publica/blog/'.$blog->id, $blog->nombre) ?></li>
                <?php endforeach;
                else: ?>
                  <li><?php echo anchor('#', '-Sin blogs-') ?></li>
                <?php endif; ?> <!-- /listado -->
              </ul>
            </li>
            <li>
            </li>

          <?php else: ?>
            <!-- Parte pública -->
            <li><?php echo anchor('login/form_login', 'Log in'); ?></li>
            <li><?php echo anchor('login/form_signup', 'Sign up'); ?></li>
          <?php endif; ?>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </div>
</nav>
