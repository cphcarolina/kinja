<!-- Cabecera -->
<?php $this->load->view('inc/cabecera'); ?>


<main class="container">
	<div class="row">
		<div class="col-md-4" id="lateral">
			<h3 class="alert alert-warning">LATERAL</h3>
		</div><!-- /lateral -->

		<div class="col-md-8" id="contenido">
			<h3 class="alert alert-warning">CONTENIDO</h3>
		</div><!-- /contenido -->

	</div>

<!-- Pie de página -->
<?php $this->load->view('inc/pie'); ?>